﻿<?php
class PDF extends FPDF
{
	//Page header
	function Header()
	{
                $this->setFont('Arial','',10);
                $this->setFillColor(255,255,255);
                $this->cell(100,6,"Laporan Perkembangan Customer BSM Griya",0,0,'L',1); 
                $this->cell(0,6,"Printed date : " . date('d/m/Y'),0,1,'R',1); 
                $this->Image(base_url().'assets/image/logobulatmoncustrans.png', 10, 25,'20','20','png');
                
                $this->Ln(12);
                $this->setFont('Arial','',14);
                $this->setFillColor(255,255,255);
                $this->cell(120,6,'',0,0,'C',0); 
                $this->cell(100,6,'Laporan Perkembangan Customer BSM Griya',0,1,'C',1); 
                $this->cell(120,6,'',0,0,'C',0); 
                $this->cell(100,6,"Periode : ".date('M Y'),0,1,'C',1); 
                $this->cell(25,6,'',0,0,'C',0); 
                
                $this->Ln(15);
                $this->setFont('Arial','',12);
                $this->setFillColor(0,146,63);
				$this->setTextColor(255,255,255);
                $this->cell(10,10,'No.',1,0,'C',1);
                $this->cell(50,10,'Nama Nasabah',1,0,'C',1);
                $this->cell(30,10,'Limit Biaya',1,0,'C',1);
                $this->cell(30,10,'Jangka Waktu',1,0,'C',1);
				$this->cell(30,10,'Hasil Analisa',1,0,'C',1);
				$this->cell(30,10,'Berkas Masuk',1,0,'C',1);
				$this->cell(30,10,'Tanggal SPPP',1,0,'C',1);
				$this->cell(30,10,'Tanggal Akad',1,0,'C',1);
				$this->cell(50,10,'Tanggal Pencairan Awal',1,0,'C',1);
				$this->cell(50,10,'Nominal Pencairan Awal',1,1,'C',1);
                
	}
 
	function Content($data)
	{
            $ya = 46;
            $rw = 6;
            $no = 1;
                foreach ($data as $key) {
						$this->Ln(15);
                        $this->setFont('Arial','',12);
                        $this->setFillColor(255,255,255);	
                        $this->cell(10,10,$no,1,0,'L',1);
                        $this->cell(50,10,$key->namanasabah,1,0,'L',1);
                        $this->cell(30,10,$key->limitbiaya,1,0,'L',1);
                        $this->cell(30,10,$key->jangkawkt,1,0,'L',1);
						$this->cell(30,10,$key->hasil,1,0,'L',1);
						$this->cell(30,10,$key->tglberkasmasuk,1,0,'L',1);
						$this->cell(30,10,$key->tglsppp,1,0,'L',1);
						$this->cell(30,10,$key->tglakad,1,0,'L',1);
						$this->cell(50,10,$key->tglcair1,1,0,'L',1);
						$this->cell(50,10,$key->nominalcair1,1,1,'L',1);
                        $ya = $ya + $rw;
                        $no++;
                }            

	}
	function Footer()
	{
		//atur posisi 1.5 cm dari bawah
		$this->SetY(-15);
		//buat garis horizontal
		$this->Line(10,$this->GetY(),340,$this->GetY());
		//Arial italic 9
		$this->SetFont('Arial','I',9);
                $this->Cell(0,10,'i-Con BSM Griya ' . date('Y'),0,0,'L');
		//nomor halaman
		$this->Cell(0,10,'Halaman '.$this->PageNo().' dari {nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($data);
$pdf->Output();
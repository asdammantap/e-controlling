<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="Asdam Wong Mantap" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>i-Con BSM Griya</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="../../assets/punyaadmin/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME STYLE  -->
    <link href="../../assets/punyaadmin/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="../../assets/punyaadmin/assets/css/style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />-->
	<link rel="icon" type="image/png" id="favicon"
          href="../../assets/image/logobulatmoncustrans.png"/>
		   <style>
		table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

@media (max-width: 30em){

    table.responsive-table{
      box-shadow: none;  
	  width:100%;
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before,
  table.responsive-table td:nth-child(3):before,
  table.responsive-table td:nth-child(4):before,
  table.responsive-table td:nth-child(5):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
}
</style>

</head>
<body>
    <div class="navbar navbar-inverse set-radius-zero" >
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">
				<img src="../../assets/image/logomoncus.jpg" style="height:70px;"/>
				</a>
            </div>
			<img src="../../assets/image/bsmtrans.png" style="height:70px;float:right;margin-top:15px;"/>
           <div class="right-div" style="float:right;color:#fff;margin-top:15px;">
			Hello, You Are Logged in As <?php echo $this->session->userdata('username'); ?>
            </div>
			
                
        </div>
    </div><!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row ">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
					<ul id="menu-top" class="nav navbar-nav navbar-left">
                            <li><a href="./adminpage" class="menu-top-active">Dashboard</a></li>
							 <li>
                                <a href="" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">Master Data <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewnas">Data Nasabah</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewdeveloper">Data Developer</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewuser">Data Pengguna</a></li>
                                </ul>
                            </li>
							 <li>
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">Proses Pendataan Developer <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewspr">SPR</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewdp">DP</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewsertifikat">Sertifikat & IMB</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewkondisi">Kondisi Bangunan</a></li>
                                </ul>
                            </li>
                           <li>
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">Proses Pembiayaan <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewcollect">Collect Data</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewanalisa">Analisa</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewsp3">SP3</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewakad">Akad</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewpencairan">Pencairan</a></li>
                                </ul>
                            </li>
                           </ul>
						<div class="right-div">
                <a href="./logout" class="btn btn-danger pull-right">LOG ME OUT</a>
            </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
     <!-- MENU SECTION END-->
    <div class="content-wrapper">
         <div class="container">
        <div class="row">
             <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color:#01573c;color:#fff;font-weight:500;">
                            Data Nasabah
                        </div>
                        <div class="panel-body">
                            <ul class="nav nav-tabs" style="color:#000;font-weight:500;">
                                <li class="active"><a href="#input" data-toggle="tab">Input Data</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="input" style="margin-left:10px;margin-top:20px;">
								<?=$this->session->flashdata('pesan')?>
			<form action="<?=base_url()?>admin/upload/insert" method="post" enctype="multipart/form-data">
								<table class="layout display responsive-table">
							<tr><td width="200px;">No. KTP</td><td width="500px;"><input type="text" name="noktp" class="form-control" placeholder="No. KTP"></td><td rowspan="8"><img src="" id="gambar_nodin" style="width:100%;height:30%;"></td></tr>
    <tr><td>Nama Lengkap : </td><td><input type="text" required  name="namanasabah" class="form-control" placeholder="Nama Lengkap"></td></tr>
	<tr><td>Jenis Kelamin : </td><td><select  name="jeniskel" class="form-control" placeholder="Jenis Kelamin">
			<option> Laki - Laki </option>
			<option> Perempuan </option>
			</select></td></tr>
	<tr><td>Tempat Lahir : </td><td><input type="text" required  name="tmptlahir" class="form-control" placeholder="Tempat Lahir"></td></tr>
	<tr><td>Tanggal Lahir :</td><td><input type="date" required  name="tgllahir" class="form-control" placeholder="Tanggal Lahir">
			</td></tr>
	<tr><td>Alamat : </td><td><textarea required  name="alamat" class="form-control" placeholder="Alamat"></textarea></td></tr>
			<tr><td>No Telp :</td><td><input type="text" required  name="notelp" class="form-control" placeholder="No. Telp">
			</td></tr>
	<tr><td>Tanggung Jawab Dari Developer :</td><td><select  name="iddeveloper" class="form-control" placeholder="Nasabah">
			<option value="None"></option>
			<option value=<?=form_dropdown('iddeveloper',$tampildatadeveloper);?></option>
			</select>
			</td></tr>
	<tr><td colspan="2"><input type="submit" class="btn btn-primary" value="Save >>"></td><td><input type="file" name="fotodiri" id="preview_gambar" /></td></tr>
						</table>         
</form>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
          </div>
					 <!--/.ROW-->
                 </div>
              </div>
     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; 2016 i-Con of BSM Griya 
                </div>

            </div>
        </div>
    </section>
      <!-- FOOTER SECTION END-->
	 
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
	<script src="../../assets/jsadmin/jquery-1.11.1.js"></script>
    <script src="../../assets/punyaadmin/assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="../../assets/punyaadmin/assets/js/bootstrap.js"></script>
      <!-- CUSTOM SCRIPTS  -->
    <script src="../../assets/punyaadmin/assets/js/custom.js"></script>
	 <script type="text/javascript">
	  function bacaGambar(input) {
   if (input.files && input.files[0]) {
      var reader = new FileReader();
 
      reader.onload = function (e) {
          $('#gambar_nodin').attr('src', e.target.result);
      }
 
      reader.readAsDataURL(input.files[0]);
   }
}
$("#preview_gambar").change(function(){
   bacaGambar(this);
});
	  </script>
  
</body>
</html>

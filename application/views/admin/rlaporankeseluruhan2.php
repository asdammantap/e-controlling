﻿    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="Asdam Wong Mantap" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>i-Con BSM Griya</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="../../assets/punyaadmin/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME STYLE  -->
    <link href="../../assets/punyaadmin/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="../../assets/punyaadmin/assets/css/style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />-->
	<link rel="icon" type="image/png" id="favicon"
          href="../../assets/image/logobulatmoncustrans.png"/>
		   <style>
		table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}
table.display td.title{
  border: 1px solid black;
  padding: .5em 1em;
}
table.display th{ 
background: #01573c;
color:#fff; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

@media (max-width: 30em){

    table.responsive-table{
      box-shadow: none;  
	  width:100%;
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before,
  table.responsive-table td:nth-child(3):before,
  table.responsive-table td:nth-child(4):before,
  table.responsive-table td:nth-child(5):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
}
</style>

<body>
   <font style="text-align:center;font-weight:bold;text-transform:uppercase;"><h1>LAPORAN PERKEMBANGAN CUSTOMER BSM GRIYA</h1></font>
   
									 <table class="layout display responsive-table">
    <thead>
		<tr>
			<th style="width:15%;height:20px;padding-top:3px;padding-left:10px;padding-bottom:3px;">Nama Nasabah</th>
			<th style="width:10%;height:20px;padding-top:3px;padding-left:10px;padding-bottom:3px;">Limit Biaya</th>
			<th style="width:15%;height:20px;padding-top:3px;padding-left:10px;padding-bottom:3px;">Tanggal Pencairan 1</th>
            <th style="width:15%;height:20px;padding-top:3px;padding-left:10px;padding-bottom:3px;">Nominal Pencairan 1</th> 
								                         
        </tr>
    </thead>
    <tbody>
					<?php 
					foreach ($data as $row) {	?> 
									<tr><td style="height:20px;padding-top:3px;padding-left:10px;padding-bottom:3px;"><?php echo $row->namanasabah?></td>
									<td style="height:20px;padding-top:3px;padding-left:10px;padding-bottom:3px;"><?php echo $row->limitbiaya?></td>
									<td style="height:20px;padding-top:3px;padding-left:10px;padding-bottom:3px;"><?php echo $row->tglcair1?></td>
									<td style="height:20px;padding-top:3px;padding-left:10px;padding-bottom:3px;"><?php echo $row->nominalcair1?></td>
									</tr>
									<?php
									}
								?>
	 
							</tbody>
						</table>         

</body>
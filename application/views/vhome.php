<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
		<title>MONCUS RAKATA-BSM</title>
	 <link rel="icon" type="image/png" id="favicon"
          href="../../assets/image/bsm.png"/>
      <link rel="stylesheet" href="../../assets/cssadmin/normalize.min.css">
      <link rel="stylesheet" href="../../assets/cssadmin/main.css">		
	  <link rel="stylesheet" href="../../assets/cssadmin/media-queries.css">		
	  <link rel="stylesheet" href="../../assets/cssadmin/bootstrap.css">
	  <link rel="stylesheet" href="../../assets/cssadmin/navbar.css">
	  <link rel="stylesheet" href="../../assets/cssadmin/bootstrap.css">	
	  <link rel="stylesheet" href="../../assets/cssadmin/bootstrap-responsive.css">	
		 <!-- Le styles -->
        
	

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="asset/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
  

      <script src="../../assets/jsadmin/vendor/modernizr-2.6.1.min.js"></script>
      
    
    <link rel="stylesheet" href="../../assets/css_ticker/style.css">  

    <script type="text/javascript" src="../../assets/js_ticker/jquery.min.js"></script>
	<script type="text/javascript" src="../../assets/js_ticker/jquery.totemticker.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#vertical-ticker').totemticker({
				row_height	:	'100px',
				next		:	'#ticker-next',
				previous	:	'#ticker-previous',
				stop		:	'#stop',
				start		:	'#start',
				mousestop	:	true,
			});
		});
	</script>
</head>

<body>
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
		
		
 	 <div class="navbar navbar-inverse">
          <div class="navbar-inner">
            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="brand" href="#">MONCUS RAKATA-BSM</a>
            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
             <div class="nav-collapse collapse">
              <ul class="nav">
                <li><a href="#">Home</a></li>
				 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Persyaratan <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Syarat Pembiayaaan</a></li>
					<li class="divider"></li>
                    <li><a href="#">Angsuran</a></li>
				  </ul>
                </li>
				 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Proses Pembiayaan <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Collect Data</a></li>
					<li class="divider"></li>
                    <li><a href="#">Analisa</a></li>
					<li class="divider"></li>
                    <li><a href="#">SP3</a></li>
					<li class="divider"></li>
					<li><a href="#">AKAD</a></li>
					<li class="divider"></li>
                    <li><a href="#">Pencairan</a></li>
                    </ul>
                </li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div><!-- /.navbar-inner -->
        </div><!-- /.navbar -->
 <center>
          <div class="span12" style="height:500px;border:1px solid black;border:1px solid #abb4c2;
  box-shadow: 1px 1px 2px rgba(0,0,0,.3);">
			<h3>Selamat Datang Di Halaman Utama MONCUS</h3>      
			</div>
		   <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->
        <script>window.jQuery || document.write('<script src="../../assets/jsadmin/vendor/jquery-1.8.2.min.js"><\/script>')</script>

        <script src="../../assets/jsadmin/plugins.js"></script>
        <script src="../../assets/jsadmin/main.js"></script>
		
		 <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../assets/jsadmin/jquery.js"></script>
    <script src="../../assets/jsadmin/bootstrap-transition.js"></script>
    <script src="../../assets/jsadmin/bootstrap-alert.js"></script>
    <script src="../../assets/jsadmin/bootstrap-modal.js"></script>
    <script src="../../assets/jsadmin/bootstrap-dropdown.js"></script>
    <script src="../../assets/jsadmin/bootstrap-scrollspy.js"></script>
    <script src="../../assets/jsadmin/bootstrap-tab.js"></script>
    <script src="../../assets/jsadmin/bootstrap-tooltip.js"></script>
    <script src="../../assets/jsadmin/bootstrap-popover.js"></script>
    <script src="../../assets/jsadmin/bootstrap-button.js"></script>
    <script src="../../assets/jsadmin/bootstrap-collapse.js"></script>
    <script src="../../assets/jsadmin/bootstrap-carousel.js"></script>
    <script src="../../assets/jsadmin/bootstrap-typeahead.js"></script>
</body>
</html>
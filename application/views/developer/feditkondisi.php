<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="Asdam Wong Mantap" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>i-Con BSM Griya</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="../../../assets/punyaadmin/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME STYLE  -->
    <link href="../../../assets/punyaadmin/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="../../../assets/punyaadmin/assets/css/style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />-->
	<link rel="icon" type="image/png" id="favicon"
          href="../../../assets/image/logobulatmoncustrans.png"/>
		   <style>
		table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

@media (max-width: 30em){

    table.responsive-table{
      box-shadow: none;  
	  width:100%;
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before,
  table.responsive-table td:nth-child(3):before,
  table.responsive-table td:nth-child(4):before,
  table.responsive-table td:nth-child(5):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
}
</style>

</head>
<body>
    <div class="navbar navbar-inverse set-radius-zero" >
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">
				<img src="../../../assets/image/logomoncus.jpg" style="height:70px;"/>
				</a>
            </div>
			<img src="../../../assets/image/bsmtrans.png" style="height:70px;float:right;margin-top:15px;"/>
           <div class="right-div" style="float:right;color:#fff;margin-top:15px;">
			Hello, You Are Logged in As <?php echo $this->session->userdata('username'); ?>
            </div>
			
                
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row ">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
					<ul id="menu-top" class="nav navbar-nav navbar-left">
                            <li><a href="../../../developer/home/developerpage" class="menu-top-active">Dashboard</a></li>
							 <li>
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">Master Data <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="../../../developer/home/viewnas">Data Nasabah</a></li>
                                 </ul>
                            </li>
                           <li>
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">Proses Pendataan Developer <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="../../../developer/home/viewspr">SPR</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="../../../developer/home/viewdp">DP</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" href="../../../developer/home/viewsertifikat">Sertifikat & IMB</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="../../../developer/home/viewkondisi">Kondisi Bangunan</a></li>
                                </ul>
                            </li>
							<li>
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">Proses Pembiayaan <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="../../../developer/home/viewcollect">Collect Data</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="../../../developer/home/viewanalisa">Analisa</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" href="../../../developer/home/viewsp3">SP3</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="../../../developer/home/viewakad">Akad</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" href="../../../developer/home/viewpencairan">Pencairan</a></li>
                                </ul>
                            </li>
                           </ul>
						<div class="right-div">
                <a href="./logout" class="btn btn-danger pull-right">LOG ME OUT</a>
            </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
     <!-- MENU SECTION END-->
    <div class="content-wrapper">
         <div class="container">
        <div class="row">
             <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color:#01573c;color:#fff;font-weight:500;">
                            Data Kondisi Rumah
                        </div>
                        <div class="panel-body">
                            <ul class="nav nav-tabs" style="color:#000;font-weight:500;">
                                <li class="active"><a href="#input" data-toggle="tab">Edit Data</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="input" style="margin-left:10px;margin-top:20px;">
								<?=$this->session->flashdata('pesan')?>
			<form action="<?=base_url()?>upload/proseseditkondisi" method="post" enctype="multipart/form-data">
			<?php foreach($data->result() as $edit):?>
								<table class="layout display responsive-table">
								<tr><td width="200px;" style="display:none;"></td><td width="500px;" style="display:none;"><input type="text" required  name="nokondisi" class="form-control" placeholder="No. Kondisi" value="<?php echo($edit->nokondisi);?>" style="display:none;"></td></tr>
							<tr><td width="200px;" style="display:none;"></td><td width="500px;" style="display:none;"><input type="text" name="nospr" class="form-control" placeholder="No. SPR" value="<?php echo($edit->nospr);?>" style="display:none"></td><td style="display:none;"><font style="font-weight:bold;text-align:center;"></font></tr>
							<tr><td width="200px;">No. SPR</td><td colspan="2"><select  name="nospr" class="form-control" placeholder="No. SPR">
							<option value=<?=$edit->nospr?> selected><?=$edit->nospr?></option>
							<option value=<?=$edit->nospr?>>--Opsi Ubah No.SPR ini--</option>
							<option value=<?=form_dropdown('nospr',$tampildataspr);?></option>
							</select></td><td rowspan="2"><center><img src="../../../assets/archive/kondisi/<?php echo($edit->kondisi);?>" id="gambar_nodin" style="width:200px;height:200px;"></td></tr>
							<tr><td colspan="2"><input type="submit" class="btn btn-primary" value="Save >>"></td><td><input type="file" name="kondisi" id="preview_gambar" value="<?php echo($edit->kondisi);?>"/>* Maximal Size 1 Mb</td></tr>
						</table>   
<?php endforeach; ?>						
</form>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
          </div>
					 <!--/.ROW-->
                 </div>
              </div>
     <!-- CONTENT-WRAPPER SECTION END-->
   <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; 2016 i-Con of BSM Griya 
                </div>

            </div>
        </div>
    </section>
      <!-- FOOTER SECTION END-->
	 
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
	<script src="../../../assets/jsadmin/jquery-1.11.1.js"></script>
    <script src="../../../assets/punyaadmin/assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="../../../assets/punyaadmin/assets/js/bootstrap.js"></script>
      <!-- CUSTOM SCRIPTS  -->
    <script src="../../../assets/punyaadmin/assets/js/custom.js"></script>
	 <script type="text/javascript">
	  function bacaGambar(input) {
   if (input.files && input.files[0]) {
      var reader = new FileReader();
 
      reader.onload = function (e) {
          $('#gambar_nodin').attr('src', e.target.result);
      }
 
      reader.readAsDataURL(input.files[0]);
   }
}
$("#preview_gambar").change(function(){
   bacaGambar(this);
});
	  </script>
  
</body>
</html>
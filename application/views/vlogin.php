<html>
<head>
<title>i-Con BSM Griya</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
 <script src="js/bootstrap.js"></script>
 <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.css">
 <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
<script src="assets/js/login.js"></script>
<link rel="stylesheet" href="assets/css/style.css">
<!--[if lt IE 9]>
 <script src=”https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js”></script>
   <script src=”https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js”></script>
<![endif]-->    
<link rel="icon" type="image/png" id="favicon"
          href="assets/image/logobulatmoncustrans.png"/>
</head>
<body>
    <div class="container">
	<div class="banner"><center>
	<img src="assets/image/logomoncus.jpg" style="height:150%;">
	</div>
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="assets/image/user.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <?php echo form_open("auth/cek_loginadmin"); ?>
                <input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
                </br><input type="password" class="form-control" placeholder="Password" name="password" required>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
				<?php echo form_close(); ?>
           <a href="#" class="forgot-password">
                Forgot the password?
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->
</body>
</html>

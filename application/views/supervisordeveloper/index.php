﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="Asdam Wong Mantap" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>i-Con BSM Griya</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="../../assets/punyaadmin/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME STYLE  -->
    <link href="../../assets/punyaadmin/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="../../assets/punyaadmin/assets/css/style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />-->
	<link rel="icon" type="image/png" id="favicon"
          href="../../assets/image/logobulatmoncustrans.png"/>
</head>
<body>
    <div class="navbar navbar-inverse set-radius-zero" >
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">
				<img src="../../assets/image/logomoncus.jpg" style="height:70px;"/>
				</a>
            </div>
			<img src="../../assets/image/bsmtrans.png" style="height:70px;float:right;margin-top:15px;"/>
           <div class="right-div" style="float:right;color:#fff;margin-top:15px;">
			Hello, You Are Logged in As <?php echo $this->session->userdata('username'); ?>
            </div>
			
                
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row ">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
					<ul id="menu-top" class="nav navbar-nav navbar-left">
                            <li><a href="" class="menu-top-active">Dashboard</a></li>
							 <li>
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">Master Data <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewnas">Data Nasabah</a></li>
                                </ul>
                            </li>
                           <li>
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">Proses Pendataan Developer <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewspr">SPR</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewdp">DP</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewsertifikat">Sertifikat & IMB</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewkondisi">Kondisi Bangunan</a></li>
                                </ul>
                            </li>
							 <li>
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">Proses Pembiayaan <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewcollect">Collect Data</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewanalisa">Analisa</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewsp3">SP3</a></li>
                                     <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewakad">Akad</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" href="./viewpencairan">Pencairan</a></li>
                                </ul>
                            </li>
                           </ul>
						<div class="right-div">
                <a href="./logout" class="btn btn-danger pull-right">LOG ME OUT</a>
            </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
     <!-- MENU SECTION END-->
    <div class="content-wrapper">
         <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">SUPERVISOR ADMIN DEVELOPER DASHBOARD</h4>
                
                            </div>

        </div>
         
		 <div class="row">
            <center>
			  <div class="col-md-3 col-sm-3 col-xs-6">
                     <div class="alert alert-danger back-widget-set text-center">
                            <i class="fa fa-envelope fa-5x"></i>
                            <h3><a href="">Message</a></h3>
                           Kirim Pesan Ke Sesama User
                        </div>
                    </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-info back-widget-set text-center">
                            <i class="fa fa-briefcase fa-5x"></i>
                            <h3><a href="./viewsyarat">Syarat</a></h3>
                           Persyaratan Pembiayaan BSM
                        </div>
                    </div>
              <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-success back-widget-set text-center">
                            <i class="fa fa-bars fa-5x"></i>
                            <h3><a href="./viewpricelist">Price List</a></h3>
                            Price List Harga Rumah
                        </div>
                    </div>
               <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-warning back-widget-set text-center">
                            <i class="fa fa-recycle fa-5x"></i>
                            <h3><a href="">Simulasi</a></h3>
                           Tentang Simulasi Angsuran
                        </div>
                    </div>
        </div>
		 
            </div>
    </div>
     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; 2016 i-Con of BSM Griya 
                </div>

            </div>
        </div>
    </section>
	<!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="../../assets/punyaadmin/assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="../../assets/punyaadmin/assets/js/bootstrap.js"></script>
      <!-- CUSTOM SCRIPTS  -->
    <script src="../../assets/punyaadmin/assets/js/custom.js"></script>
  
</body>
</html>

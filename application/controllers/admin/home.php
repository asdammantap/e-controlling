<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('fpdf');
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function index()
	{
		$this->load->view('admin/home');
	}
	public function adminpage()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('admin/index',$data);
	}
	public function viewsimulasi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampilsimulasiangsuran();
		$this->load->view('admin/ftampilsimulasi',$data);
	}
	public function viewreport3()
	{
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampillaporan();
		ob_start();
		$content = $this->load->view('admin/rlaporankeseluruhan',$data);
		$content = ob_get_clean();		
		$this->load->library('html2pdf');
		try
		{
			$html2pdf = new HTML2PDF('L', 'A4', 'fr');
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
			$html2pdf->Output('laporanperkembangancustomerbsmgriya.pdf');
		}
		catch(HTML2PDF_exception $e) {
			echo $e;
			exit;
		}
		
	}
	public function viewreport()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampillaporan();
		$this->load->view('admin/rlaporankeseluruhan',$data);
	}
	public function addnas()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildatadeveloper']=$this->vmodul_admin->getnamadeveloper();
		$this->load->view('admin/finputnas',$data);
	}
	public function addsimulasi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('admin/finputsimulasi',$data);
	}
	public function adddeveloper()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('admin/finputdeveloper',$data);
	}
	public function adduser()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildatadeveloper']=$this->vmodul_admin->getnamadeveloper();
		$this->load->view('admin/finputuser',$data);
	}
	public function addspr()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['no_spr']=$this->vmodul_admin->get_nospr();
		$data['tampildatanasabah']=$this->vmodul_admin->getnamanasabah();
		$data['tampildatadeveloper']=$this->vmodul_admin->getnamadeveloper();
		$this->load->view('admin/finputspr',$data);
	}
	public function adddp()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['no_dp']=$this->vmodul_admin->get_nodp();
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['tampildatadp']=$this->vmodul_admin->getdp();
		$this->load->view('admin/finputdp',$data);
	}
	public function addsertifikat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('admin/finputsertifikat',$data);
	}
	public function addkondisi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['no_kondisi']=$this->vmodul_admin->get_nokondisi();
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('admin/finputkondisi',$data);
	}
	public function addcollect()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['no_berkas']=$this->vmodul_admin->get_noberkas();
		$this->load->view('admin/finputcollect',$data);
	}
	public function addanalisa()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['tampildataberkas']=$this->vmodul_admin->getberkas();
		$data['id_analisa']=$this->vmodul_admin->get_idanalisa();
		$this->load->view('admin/finputanalisa',$data);
	}
	public function addsp3()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataberkas']=$this->vmodul_admin->getnoberkas();
		$data['tampildataanalisa']=$this->vmodul_admin->getidanalisa();
		$this->load->view('admin/finputsppp',$data);
	}
	public function syarat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('admin/finputsyarat',$data);
	}
	public function addpencairan()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildatasppp']=$this->vmodul_admin->getnosp3();
		$data['tampildatacair']=$this->vmodul_admin->getnocair();
		$data['no_cair']=$this->vmodul_admin->get_cair();
		$this->load->view('admin/finputpencairan',$data);
	}
	public function addakad()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildatasppp']=$this->vmodul_admin->getnosp3();
		$data['tampildatakondisi']=$this->vmodul_admin->getnokondisi();
		$data['no_akad']=$this->vmodul_admin->get_akad();
		$this->load->view('admin/finputakad',$data);
	}
	public function viewnas()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatanasabah();
		$this->load->view('admin/ftampilnas',$data);
	}
	public function viewdeveloper()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatadeveloper();
		$this->load->view('admin/ftampildeveloper',$data);
	}
	public function viewsyarat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasyarat();
		$this->load->view('admin/ftampilsyarat',$data);
	}
	public function viewpricelist()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatapricelist();
		$this->load->view('admin/ftampilpricelist',$data);
	}
	public function viewuser()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatauser();
		$this->load->view('admin/ftampiluser',$data);
	}
	public function viewspr()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['datakurang']=$this->vmodul_admin->tampildatakurang();
		$data['data']=$this->vmodul_admin->tampildataspr();
		$this->load->view('admin/ftampilspr',$data);
	}
	public function viewdp()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatadp();
		$this->load->view('admin/ftampildp',$data);
	}
	public function viewpencairan()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatapencairan();
		$this->load->view('admin/ftampilpencairan',$data);
	}
	public function viewsertifikat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasertifikat();
		$this->load->view('admin/ftampilsertifikat',$data);
	}
	public function viewkondisi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatakondisi();
		$this->load->view('admin/ftampilkondisi',$data);
	}
	
	public function viewcollect()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatacollect();
		$this->load->view('admin/ftampilcollect',$data);
	}
	public function viewanalisa()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildataanalisa();
		$this->load->view('admin/ftampilanalisa',$data);
	}
	public function viewsp3()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasp3();
		$this->load->view('admin/ftampilsppp',$data);
	}
	public function viewakad()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildataakad();
		$this->load->view('admin/ftampilakad',$data);
	}
	public function deletekondisi($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_akadkondisi($id);
		$data['data']=$this->vmodul_admin->hapus_kondisi($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewkondisi');
            }
		$this->load->view('developer/ftampilnas', $data);
	}
	public function deletenas($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_spradmin($id);
		$data['data']=$this->vmodul_admin->hapus_nasabah($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewnas');
            }
		$this->load->view('admin/ftampilnas', $data);
	}
	public function deletedeveloper($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_nasabahdeveloper($id);
		$data['data']=$this->vmodul_admin->hapus_developer($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewdeveloper');
            }
		$this->load->view('admin/ftampildeveloper', $data);
	}
	public function deletecollect($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_analisaberkas($id);
		$data['data']=$this->vmodul_admin->hapus_berkas($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewcollect');
            }
		$this->load->view('admin/ftampilcollect', $data);
	}
	public function deleteanalisa($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_spppanalisa($id);
		$data['data']=$this->vmodul_admin->hapus_analisa($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewanalisa');
            }
		$this->load->view('admin/ftampilanalisa', $data);
	}
	public function deletecair($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE);
		$data['data']=$this->vmodul_admin->hapus_cair($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewpencairan');
            }
		$this->load->view('admin/ftampilcair', $data);
	}
	public function deleteakad($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE);
		$data['data']=$this->vmodul_admin->hapus_akad($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewakad');
            }
		$this->load->view('admin/ftampilakad', $data);
	}
	public function deletesp3($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE);
		$data['data']=$this->vmodul_admin->hapus_cairanalisa($id);
		$data['data']=$this->vmodul_admin->hapus_akadanalisa($id);
		$data['data']=$this->vmodul_admin->hapus_sppp($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewsp3');
            }
		$this->load->view('admin/ftampilanalisa', $data);
	}
	public function deleteuser($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		/*$data['data']=$this->vmodul_admin->hapus_nasabahdeveloper($id);*/
		$data['data']=$this->vmodul_admin->hapus_developeruser($id);
		$data['data']=$this->vmodul_admin->hapus_userbsm($id);
		$data['data']=$this->vmodul_admin->hapus_user($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewuser');
            }
		$this->load->view('admin/ftampiluser', $data);
	}
	public function deletespr($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_spr($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data SPR Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewspr');
            }
		$this->load->view('admin/ftampilspr', $data);
	}
	public function deletedp($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_dp($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data DP Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/viewdp');
            }
		$this->load->view('admin/ftampildp', $data);
	}
	public function editspr($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editspr($id);
		$data['tampildatanasabah']=$this->vmodul_admin->getnamanasabah();
		$data['tampildatadeveloper']=$this->vmodul_admin->getnamadeveloper();
		$this->load->view('admin/feditspr', $data);
        
    }
	public function detailspr($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editspr($id);
		$this->load->view('admin/fdetailspr', $data);
        
    }
	public function editcair($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editcair($id);
		$data['tampildatasppp']=$this->vmodul_admin->getnosp3();
		$data['tampildatacair']=$this->vmodul_admin->getnocair();
		$this->load->view('admin/feditcair', $data);
        
    }
	public function editsyarat($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editsyarat($id);
		$this->load->view('admin/feditsyarat', $data);
        
    }
	public function editdp($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editdp($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['tampildatadp']=$this->vmodul_admin->getdp();
		$this->load->view('admin/feditdp', $data);
        
    }
	public function editkondisi($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editkondisi($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('admin/feditkondisi', $data);
        
    }
	public function detaildp($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editdp($id);
		$this->load->view('admin/fdetaildp', $data);
        
    }
	public function detailsertifikat($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_detailsertifikat($id);
		$this->load->view('admin/fdetailsertifikat', $data);
        
    }
	public function editprofiladmin($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('mupload');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->mupload->get_editadmin($id);
		$data['tampildatadeveloper']=$this->vmodul_admin->getnamadeveloper();
		$this->load->view('admin/feditnas', $data);
	}
	public function formupload()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('admin/fupload',$data);
	}
	 
	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('');
	}

}

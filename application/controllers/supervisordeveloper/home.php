<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function index()
	{
		$this->load->view('supervisordeveloper/home');
	}
	public function supervisorpage()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('supervisordeveloper/index',$data);
	}
	public function viewreport()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampillaporan();
		$this->load->view('supervisordeveloper/rlaporankeseluruhan',$data);
	}
	public function viewcollect()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatacollect();
		$this->load->view('supervisordeveloper/ftampilcollect',$data);
	}
	public function viewpencairan()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatapencairan();
		$this->load->view('supervisordeveloper/ftampilpencairan',$data);
	}
	public function viewanalisa()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildataanalisa();
		$this->load->view('supervisordeveloper/ftampilanalisa',$data);
	}
	public function viewsp3()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasp3();
		$this->load->view('supervisordeveloper/ftampilsppp',$data);
	}
	public function viewakad()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildataakad();
		$this->load->view('supervisordeveloper/ftampilakad',$data);
	}
	public function addnas()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('supervisordeveloper/finputnas',$data);
	}
	public function addspr()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['no_spr']=$this->vmodul_admin->get_nospr();
		$data['tampildatasupervisordeveloper']=$this->vmodul_admin->getnamasupervisordeveloper();
		$data['tampildatanasabah']=$this->vmodul_admin->getnamanasabah();
		$this->load->view('supervisordeveloper/finputspr',$data);
	}
	public function adddp()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['no_dp']=$this->vmodul_admin->get_nodp();
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['tampildatadp']=$this->vmodul_admin->getdp();
		$this->load->view('supervisordeveloper/finputdp',$data);
	}
	public function addsertifikat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('supervisordeveloper/finputsertifikat',$data);
	}
	public function addkondisi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['no_kondisi']=$this->vmodul_admin->get_nokondisi();
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('supervisordeveloper/finputkondisi',$data);
	}
	public function viewnas()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatanasabah();
		$this->load->view('supervisordeveloper/ftampilnas',$data);
	}
	public function viewspr()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildataspr();
		$this->load->view('supervisordeveloper/ftampilspr',$data);
	}
	public function pricelist()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('supervisordeveloper/finputpricelist',$data);
	}
	public function viewsyarat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasyarat();
		$this->load->view('supervisordeveloper/ftampilsyarat',$data);
	}
	public function viewpricelist()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatapricelist();
		$this->load->view('supervisordeveloper/ftampilpricelist',$data);
	}
	public function viewdp()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatadp();
		$this->load->view('supervisordeveloper/ftampildp',$data);
	}
	public function viewsertifikat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasertifikat();
		$this->load->view('supervisordeveloper/ftampilsertifikat',$data);
	}
	public function viewkondisi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatakondisi();
		$this->load->view('supervisordeveloper/ftampilkondisi',$data);
	}
	public function deletenas($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_nasabah($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../supervisordeveloper/home/viewnas');
            }
		$this->load->view('supervisordeveloper/ftampilnas', $data);
	}
	public function deletespr($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_dpspr($id);
		$data['data']=$this->vmodul_admin->hapus_sertifikat($id);
		$data['data']=$this->vmodul_admin->hapus_analisaspr($id);
		$data['data']=$this->vmodul_admin->hapus_berkasspr($id);
		$data['data']=$this->vmodul_admin->hapus_kondisispr($id);
		$data['data']=$this->vmodul_admin->hapus_spr($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data SPR Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../supervisordeveloper/home/viewspr');
            }
		$this->load->view('supervisordeveloper/ftampilspr', $data);
	}
	public function deletedp($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_dp($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data DP Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../supervisordeveloper/home/viewdp');
            }
		$this->load->view('supervisordeveloper/ftampildp', $data);
	}
	public function editspr($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editspr($id);
		$data['tampildatanasabah']=$this->vmodul_admin->getnamanasabah();
		$this->load->view('supervisordeveloper/feditspr', $data);
        
    }
	public function editpricelist($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editpricelist($id);
		$this->load->view('supervisordeveloper/feditpricelist', $data);
        
    }
	public function editsertifikat($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editsertifikat($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('supervisordeveloper/feditsertifikat', $data);
        
    }
	public function editimb($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editimb($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('supervisordeveloper/feditimb', $data);
        
    }
	public function editdp($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editdp($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['tampildatadp']=$this->vmodul_admin->getdp();
		$this->load->view('supervisordeveloper/feditdp', $data);
        
    }
	public function editkondisi($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editkondisi($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('supervisordeveloper/feditkondisi', $data);
        
    }
	public function formupload()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('supervisordeveloper/fupload',$data);
	}
	 
	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('');
	}

}

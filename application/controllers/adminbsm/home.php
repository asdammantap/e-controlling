<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function index()
	{
		$this->load->view('adminbsm/home');
	}
	public function adminpage()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('adminbsm/index',$data);
	}
	public function viewreport()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampillaporan();
		$this->load->view('adminbsm/rlaporankeseluruhan',$data);
	}
	public function addnas()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('adminbsm/finputnas',$data);
	}
	public function adddeveloper()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('adminbsm/finputdeveloper',$data);
	}
	public function adduser()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildatadeveloper']=$this->vmodul_admin->getnamadeveloper();
		$this->load->view('adminbsm/finputuser',$data);
	}
	public function addspr()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataadmin']=$this->vmodul_admin->getnamaadmin();
		$data['tampildatanasabah']=$this->vmodul_admin->getnamanasabah();
		$this->load->view('adminbsm/finputspr',$data);
	}
	public function adddp()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['tampildatadp']=$this->vmodul_admin->getdp();
		$this->load->view('adminbsm/finputdp',$data);
	}
	public function addsertifikat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('adminbsm/finputsertifikat',$data);
	}
	public function addkondisi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('adminbsm/finputkondisi',$data);
	}
	public function addcollect()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['no_berkas']=$this->vmodul_admin->get_noberkas();
		$this->load->view('adminbsm/finputcollect',$data);
	}
	public function addanalisa()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['tampildataberkas']=$this->vmodul_admin->getberkas();
		$data['id_analisa']=$this->vmodul_admin->get_idanalisa();
		$this->load->view('adminbsm/finputanalisa',$data);
	}
	public function addsp3()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataberkas']=$this->vmodul_admin->getnoberkas();
		$data['tampildataanalisa']=$this->vmodul_admin->getidanalisa();
		$this->load->view('adminbsm/finputsppp',$data);
	}
	public function syarat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('adminbsm/finputsyarat',$data);
	}
	public function addpencairan()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildatasppp']=$this->vmodul_admin->getnosp3();
		$data['tampildatacair']=$this->vmodul_admin->getnocair();
		$data['no_cair']=$this->vmodul_admin->get_cair();
		$this->load->view('adminbsm/finputpencairan',$data);
	}
	public function addakad()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildatasppp']=$this->vmodul_admin->getnosp3();
		$data['tampildatakondisi']=$this->vmodul_admin->getnokondisi();
		$data['no_akad']=$this->vmodul_admin->get_akad();
		$this->load->view('adminbsm/finputakad',$data);
	}
	public function viewnas()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatanasabah();
		$this->load->view('adminbsm/ftampilnas',$data);
	}
	public function viewdeveloper()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatadeveloper();
		$this->load->view('adminbsm/ftampildeveloper',$data);
	}
	public function viewsyarat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasyarat();
		$this->load->view('adminbsm/ftampilsyarat',$data);
	}
	public function viewpricelist()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatapricelist();
		$this->load->view('adminbsm/ftampilpricelist',$data);
	}
	public function viewuser()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatauserbsm();
		$this->load->view('adminbsm/ftampiluser',$data);
	}
	public function viewspr()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasprbsm();
		$this->load->view('adminbsm/ftampilspr',$data);
	}
	public function viewdp()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatadpbsm();
		$this->load->view('adminbsm/ftampildp',$data);
	}
	public function viewpencairan()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatapencairanbsm();
		$this->load->view('adminbsm/ftampilpencairan',$data);
	}
	public function viewsertifikat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasertifikatbsm();
		$this->load->view('adminbsm/ftampilsertifikat',$data);
	}
	public function viewkondisi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatakondisibsm();
		$this->load->view('adminbsm/ftampilkondisi',$data);
	}
	public function viewcollect()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatacollectbsm();
		$this->load->view('adminbsm/ftampilcollect',$data);
	}
	public function viewanalisa()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildataanalisabsm();
		$this->load->view('adminbsm/ftampilanalisa',$data);
	}
	public function viewsp3()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasp3bsm();
		$this->load->view('adminbsm/ftampilsppp',$data);
	}
	public function viewakad()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildataakadbsm();
		$this->load->view('adminbsm/ftampilakad',$data);
	}
	public function deletenas($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_nasabah($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../adminbsm/home/viewnas');
            }
		$this->load->view('adminbsm/ftampilnas', $data);
	}
	public function deletedeveloper($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_nasabahdeveloper($id);
		$data['data']=$this->vmodul_admin->hapus_developer($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../adminbsm/home/viewdeveloper');
            }
		$this->load->view('adminbsm/ftampildeveloper', $data);
	}
	public function deletecollect($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_analisaberkas($id);
		$data['data']=$this->vmodul_admin->hapus_berkas($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../adminbsm/home/viewcollect');
            }
		$this->load->view('adminbsm/ftampilcollect', $data);
	}
	public function deleteanalisa($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_spppanalisa($id);
		$data['data']=$this->vmodul_admin->hapus_analisa($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../adminbsm/home/viewanalisa');
            }
		$this->load->view('adminbsm/ftampilanalisa', $data);
	}
	public function deletecair($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE);
		$data['data']=$this->vmodul_admin->hapus_cair($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../adminbsm/home/viewpencairan');
            }
		$this->load->view('adminbsm/ftampilcair', $data);
	}
	public function deleteakad($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE);
		$data['data']=$this->vmodul_admin->hapus_akad($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../adminbsm/home/viewakad');
            }
		$this->load->view('adminbsm/ftampilakad', $data);
	}
	public function deletesp3($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE);
		$data['data']=$this->vmodul_admin->hapus_cairanalisa($id);
		$data['data']=$this->vmodul_admin->hapus_akadanalisa($id);
		$data['data']=$this->vmodul_admin->hapus_sppp($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../adminbsm/home/viewsp3');
            }
		$this->load->view('adminbsm/ftampilanalisa', $data);
	}
	public function deleteuser($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		/*$data['data']=$this->vmodul_admin->hapus_nasabahdeveloper($id);*/
		$data['data']=$this->vmodul_admin->hapus_developeruser($id);
		$data['data']=$this->vmodul_admin->hapus_user($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../adminbsm/home/viewuser');
            }
		$this->load->view('adminbsm/ftampiluser', $data);
	}
	public function deletespr($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_spr($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data SPR Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../adminbsm/home/viewspr');
            }
		$this->load->view('adminbsm/ftampilspr', $data);
	}
	public function deletedp($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_dp($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data DP Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../adminbsm/home/viewdp');
            }
		$this->load->view('adminbsm/ftampildp', $data);
	}
	public function editspr($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editspr($id);
		$data['tampildatanasabah']=$this->vmodul_admin->getnamanasabah();
		$this->load->view('adminbsm/feditspr', $data);
        
    }
	public function editcair($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editcair($id);
		$data['tampildatasppp']=$this->vmodul_admin->getnosp3();
		$data['tampildatacair']=$this->vmodul_admin->getnocair();
		$this->load->view('adminbsm/feditcair', $data);
        
    }
	public function editsyarat($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editsyarat($id);
		$this->load->view('adminbsm/feditsyarat', $data);
        
    }
	public function editdp($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editdp($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['tampildatadp']=$this->vmodul_admin->getdp();
		$this->load->view('adminbsm/feditdp', $data);
        
    }
	public function formupload()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('adminbsm/fupload',$data);
	}
	 
	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('');
	}

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller
{
    var $limit=10;
	var $offset=10;

    public function __construct() {
        parent::__construct();
        $this->load->model('mupload'); //load model mupload yang berada di folder model
        $this->load->helper(array('url')); //load helper url 

    }

	public function index($page=NULL,$offset='',$key=NULL)
	{
        $data['titel']='Upload CodeIgniter'; //varibel title
        
        $data['query'] = $this->mupload->get_allimage(); //query dari model
        
        $this->load->view('vupload',$data); //tampilan awal ketika controller upload di akses
	}
    public function insert(){
        $this->load->library('upload');
        $nmfile = $this->input->post('noktp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/uploads/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotodiri']['name'])
        {
            if ($this->upload->do_upload('fotodiri'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'noktp' =>$this->input->post('noktp'),
				  'namanasabah' =>$this->input->post('namanasabah'),
				  'jeniskel' =>$this->input->post('jeniskel'),
				  'tmptlahir' =>$this->input->post('tmptlahir'),
				  'tgllahir' =>$this->input->post('tgllahir'),
				  'alamat' =>$this->input->post('alamat'),
				  'notelp' =>$this->input->post('notelp'),
                  'foto' =>$gbr['file_name'],
				  'idadmin' =>$this->session->userdata('idadmin')
                );

                $this->mupload->get_insert($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Nasabah Berhasil !!</div></div>");
                redirect('../adminbsm/home/addnas'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Nasabah Berhasil</div></div>");
                redirect('../adminbsm/home/addnas'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
	public function insertspr(){
        $this->load->library('upload');
        $nmfile = 'spr_'.$this->input->post('noktp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/spr'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotospr']['name'])
        {
            if ($this->upload->do_upload('fotospr'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'nospr' =>$this->input->post('nospr'),
				  'noktp' =>$this->input->post('noktp'),
				  'alamatobjek' =>$this->input->post('alamatobjek'),
				  'hargabangunan' =>$this->input->post('hargabangunan'),
				  'plafonbiaya' =>$this->input->post('plafonbiaya'),
				  'jangkawkt' =>$this->input->post('jangkawkt'),
                  'fotospr' =>$gbr['file_name'],
				  'idadmin' =>$this->session->userdata('idadmin')
                );

                $this->mupload->get_insertspr($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data SPR Berhasil !!</div></div>");
                redirect('../adminbsm/home/addspr'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data SPR Gagal !!</div></div>");
                redirect('../adminbsm/home/addspr'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
	public function insertsyarat(){
        $this->load->library('upload');
        $nmfile = 'syarat_'.date(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/syarat'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['syarat']['name'])
        {
            if ($this->upload->do_upload('syarat'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'syarat' =>$gbr['file_name']
                );

                $this->mupload->get_insertsyarat($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Syarat Pembiayaan Berhasil !!</div></div>");
                redirect('../adminbsm/home/syarat'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Syarat Pembiayaan Gagal !!</div></div>");
                redirect('../adminbsm/home/syarat'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
	public function insertsimulasi(){
        $this->load->library('upload');
        $nmfile = 'syarat_'.date(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/simulasi'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['syarat']['name'])
        {
            if ($this->upload->do_upload('syarat'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'syarat' =>$gbr['file_name']
                );

                $this->mupload->get_insertsyarat($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Syarat Pembiayaan Berhasil !!</div></div>");
                redirect('../adminbsm/home/syarat'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Syarat Pembiayaan Gagal !!</div></div>");
                redirect('../adminbsm/home/syarat'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
	public function insertdp1(){
        $data = array(
				  'nodp' =>$this->input->post('nodp'),
				  'nospr' =>$this->input->post('nospr'),
				  'tglbayardp1' =>$this->input->post('tglbayardp1'),
				  'nominaldp1' =>$this->input->post('nominaldp1')
				  );
		$this->mupload->get_insertdp($data); //akses model untuk menyimpan ke database
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data DP Berhasil !!</div></div>");
                redirect('../adminbsm/home/adddp'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data DP Gagal !!</div></div>");
                redirect('../adminbsm/home/adddp'); //jika gagal maka akan ditampilkan form upload
}         
    }
	public function insertcair1(){
        $data = array(
				  'nocair' =>$this->input->post('nocair'),
				  'nosppp' =>$this->input->post('nosppp'),
				  'tglcair1' =>$this->input->post('tglcair1'),
				  'nominalcair1' =>$this->input->post('nominalcair1')
				  );
		$this->mupload->get_insertcair($data); //akses model untuk menyimpan ke database
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Pencairan Berhasil !!</div></div>");
                redirect('../adminbsm/home/addpencairan'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Pencairan Gagal !!</div></div>");
                redirect('../adminbsm/home/addpencairan'); //jika gagal maka akan ditampilkan form upload
}         
    }
	public function insertakad(){
        $data = array(
				  'noakad' =>$this->input->post('noakad'),
				  'tglakad' =>$this->input->post('tglakad'),
				  'nosppp' =>$this->input->post('nosppp'),
				  'biayanotaris' =>$this->input->post('biayanotaris'),
				  'biayaasji' =>$this->input->post('biayaasji'),
				  'biayaaskar' =>$this->input->post('biayaaskar'),
				  'nokondisi' =>$this->input->post('nokondisi'),
				  );
		$this->mupload->get_insertakad($data); //akses model untuk menyimpan ke database
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Akad Berhasil !!</div></div>");
                redirect('../adminbsm/home/addakad'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Akad Gagal !!</div></div>");
                redirect('../adminbsm/home/addakad'); //jika gagal maka akan ditampilkan form upload
}         
    }
	public function insertsppp(){
	
	 $this->load->library('upload');
        $nmfile = 'sppp_'.$this->input->post('nosppp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/sppp'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotosppp']['name'])
        {
            if ($this->upload->do_upload('fotosppp'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'nosppp' =>$this->input->post('nosppp'),
				  'tglsppp' =>$this->input->post('tglsppp'),
				  'idanalisa' =>$this->input->post('idanalisa'),
				  'fotosppp' =>$gbr['file_name'],
				  'marketingbsm' =>$this->input->post('marketingbsm')
				  );
				$this->mupload->get_insertsppp($data); //akses model untuk menyimpan ke database
				//pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data SPPP Berhasil !!</div></div>");
                redirect('../adminbsm/home/addsp3'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data SPPP Gagal !!</div></div>");
                redirect('../adminbsm/home/addsp3'); //jika gagal maka akan ditampilkan form upload
            }
        }
		         
    }
	public function insertcollect(){
        $data = array(
				  'noberkas' =>$this->input->post('noberkas'),
				  'tglberkasmasuk' =>$this->input->post('tglberkasmasuk'),
				  'nospr' =>$this->input->post('nospr'),
				  'datakurang' =>$this->input->post('datakurang')
				  );
		$this->mupload->get_insertcollect($data); //akses model untuk menyimpan ke database
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Collect Data Berhasil !!</div></div>");
                redirect('../adminbsm/home/addcollect'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Collect Data Gagal !!</div></div>");
                redirect('../adminbsm/home/addcollect'); //jika gagal maka akan ditampilkan form upload
}         
    }
	public function insertanalisa(){
        $data = array(
				  'idanalisa' =>$this->input->post('idanalisa'),
				  'tglmulaianalisa' =>$this->input->post('tglmulaianalisa'),
				  'nospr' =>$this->input->post('nospr'),
				  'hasil' =>$this->input->post('hasil'),
				  'noberkas' =>$this->input->post('noberkas')
				  );
		$this->mupload->get_insertanalisa($data); //akses model untuk menyimpan ke database
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Analisa Data Berhasil !!</div></div>");
                redirect('../adminbsm/home/addanalisa'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Analisa Data Gagal !!</div></div>");
                redirect('../adminbsm/home/addanalisa'); //jika gagal maka akan ditampilkan form upload
}         
    }
	public function editdeveloper($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editdeveloper($id);
		$this->load->view('adminbsm/feditdeveloper', $data);
        
    }
	public function editcollect($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editcollect($id);
		$this->load->view('adminbsm/feditcollect', $data);
        
    }
	public function editsp3($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editsppp($id);
		$data['tampildataanalisa']=$this->vmodul_admin->getidanalisa();
		$this->load->view('adminbsm/feditsppp', $data);
        
    }
	public function editanalisa($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editanalisa($id);
		$this->load->view('adminbsm/feditanalisa', $data);
        
    }
	public function edituser($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_edituser($id);
		$this->load->view('adminbsm/fedituser', $data);
        
    }
	public function insertdeveloper(){
		$data = array(
				  'username' =>$this->input->post('username'),
				  'password' =>md5($this->input->post('password')),
				  'signas' =>"Admin Developer"
				  );
        $data1 = array(
				  'iddeveloper' =>$this->input->post('iddeveloper'),
				  'namadeveloper' =>$this->input->post('namadeveloper'),
				  'alamat' =>$this->input->post('alamat'),
				  'notelp' =>$this->input->post('notelp'),
				  'username' =>$this->input->post('username')
				  );	  
		$this->mupload->get_insertlogin($data); //akses model untuk menyimpan ke database
		$this->mupload->get_insertdeveloper($data1); //akses model untuk menyimpan ke database
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Developer Berhasil !!</div></div>");
                redirect('../adminbsm/home/adddeveloper'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Developer Gagal !!</div></div>");
                redirect('../adminbsm/home/adddeveloper'); //jika gagal maka akan ditampilkan form upload
}         
    }
	public function insertuser(){
		$data = array(
				  'username' =>$this->input->post('username'),
				  'password' =>md5($this->input->post('password')),
				  'signas' =>$this->input->post('signas')
				  );
          
		$this->mupload->get_insertlogin($data); //akses model untuk menyimpan ke database
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data User Berhasil !!</div></div>");
                redirect('../adminbsm/home/adduser'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data User Gagal !!</div></div>");
                redirect('../adminbsm/home/adduser'); //jika gagal maka akan ditampilkan form upload
}         
    }
	public function insertuserbsm(){
		$data = array(
				  'username' =>$this->input->post('username'),
				  'password' =>md5($this->input->post('password')),
				  'signas' =>$this->input->post('signas')
				  );
		$databsm = array(
				  'username' =>$this->input->post('username'),
				  'iddeveloper' =>$this->input->post('iddeveloper')
				  );
          
		$this->mupload->get_insertlogin($data); //akses model untuk menyimpan ke database
		$this->mupload->get_insertloginuserbsm($databsm); //akses model untuk menyimpan ke database
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data User BSM Berhasil !!</div></div>");
                redirect('../adminbsm/home/adduser'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data User BSM Gagal !!</div></div>");
                redirect('../adminbsm/home/adduser'); //jika gagal maka akan ditampilkan form upload
}         
    }
	public function editprofil($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('mupload');
		$data['data']=$this->mupload->get_edit($id);
		$this->load->view('adminbsm/feditnas', $data);
        
    }
	function proseseditprofil() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditprofil(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../adminbsm/home/viewnas');
        }
		function proseseditsppp() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditsp3(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../adminbsm/home/viewsp3');
        }
	function proseseditspr() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditspr(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../adminbsm/home/viewspr');
        }
		function proseseditdp() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditdp(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../adminbsm/home/viewdp');
        }
		function insertdp2() { 
		   $id = $this->input->post('nodp'); 
		   $tglbayardp2 = $this->input->post('tglbayardp2');
			$nominaldp2 = $this->input->post('nominaldp2');
			
            $data = array ( 
			
			'tglbayardp2' => $this->input->post('tglbayardp2'),
			'nominaldp2' => $this->input->post('nominaldp2'),
			); 
            $this->db->where('nodp',$id); 
            $this->db->update('moncus_dp',$data); 
			if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data DP Berhasil !!</div></div>");
                redirect('../adminbsm/home/adddp'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data DP Gagal !!</div></div>");
                redirect('../adminbsm/home/adddp'); //jika gagal maka akan ditampilkan form upload
}
			
        }
		function proseseditberkas() { 
		   $id = $this->input->post('noberkas'); 
		   $nospr = $this->input->post('nospr');
		   $tglberkasmasuk = $this->input->post('tglberkasmasuk');
			$datakurang = $this->input->post('datakurang');
			
            $data = array ( 
			'nospr' => $this->input->post('nospr'),
			'tglberkasmasuk' => $this->input->post('tglberkasmasuk'),
			'datakurang' => $this->input->post('datakurang')
			); 
            $this->db->where('noberkas',$id); 
            $this->db->update('moncus_berkas',$data); 
			if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Update Collect Data Berhasil !!</div></div>");
                redirect('../adminbsm/home/viewcollect'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Update Collect Data Gagal !!</div></div>");
                redirect('../adminbsm/home/viewcollect'); //jika gagal maka akan ditampilkan form upload
}
			
        }
		function proseseditcair() { 
		   $id = $this->input->post('nocair'); 
		   $nosppp = $this->input->post('nosppp');
		   $tglcair1 = $this->input->post('tglcair1');
			$nominalcair1 = $this->input->post('nominalcair1');
			$tglcair2 = $this->input->post('tglcair2');
			$nominalcair2 = $this->input->post('nominalcair2');
			$tglcair3 = $this->input->post('tglcair3');
			$nominalcair3 = $this->input->post('nominalcair3');
			
            $data = array ( 
			
			'nocair' =>$this->input->post('nocair'),
				  'nosppp' =>$this->input->post('nosppp'),
				  'tglcair1' =>$this->input->post('tglcair1'),
				  'nominalcair1' =>$this->input->post('nominalcair1'),
				  'tglcair2' =>$this->input->post('tglcair2'),
				  'nominalcair2' =>$this->input->post('nominalcair2'),
				  'tglcair3' =>$this->input->post('tglcair3'),
				  'nominalcair3' =>$this->input->post('nominalcair3')
			); 
            $this->db->where('nocair',$id); 
            $this->db->update('moncus_cair',$data); 
			if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Update Data Pencairan Berhasil !!</div></div>");
                redirect('../adminbsm/home/viewpencairan'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Update Data Pencairan Gagal !!</div></div>");
                redirect('../adminbsm/home/viewpencairan'); //jika gagal maka akan ditampilkan form upload
}
			
        }
		function proseseditakad() { 
		   $id = $this->input->post('noakad'); 
		   $tglakad = $this->input->post('tglakad');
		   $nosppp = $this->input->post('nosppp');
		   $biayanotaris = $this->input->post('biayanotaris');
		   $biayaasji = $this->input->post('biayaasji');
		   $biayaaskar = $this->input->post('biayaaskar');
		   $nokondisi = $this->input->post('nokondisi');
		   
             $data = array(
				  'noakad' =>$this->input->post('noakad'),
				  'tglakad' =>$this->input->post('tglakad'),
				  'nosppp' =>$this->input->post('nosppp'),
				  'biayanotaris' =>$this->input->post('biayanotaris'),
				  'biayaasji' =>$this->input->post('biayaasji'),
				  'biayaaskar' =>$this->input->post('biayaaskar'),
				  'nokondisi' =>$this->input->post('nokondisi'),
				  );
            $this->db->where('noakad',$id); 
            $this->db->update('moncus_akad',$data); 
			if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Update Data Akad Berhasil !!</div></div>");
                redirect('../adminbsm/home/viewakad'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Update Data Akad Gagal !!</div></div>");
                redirect('../adminbsm/home/viewakad'); //jika gagal maka akan ditampilkan form upload
}
			
        }
		public function editakad($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editakad($id);
		$data['tampildatasppp']=$this->vmodul_admin->getnosp3();
		$data['tampildatakondisi']=$this->vmodul_admin->getnokondisi();
		$this->load->view('adminbsm/feditakad', $data);
        
    }
		function proseseditanalisa() { 
		   $id = $this->input->post('idanalisa'); 
		   $nospr = $this->input->post('nospr');
		   $tglmulaianalisa = $this->input->post('tglmulaianalisa');
			$hasil = $this->input->post('hasil');
			
            $data = array ( 
			'tglmulaianalisa' => $this->input->post('tglmulaianalisa'),
			'nospr' => $this->input->post('nospr'),
			'hasil' => $this->input->post('hasil')
			); 
            $this->db->where('idanalisa',$id); 
            $this->db->update('moncus_analis',$data); 
			if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Update Analisa Berhasil !!</div></div>");
                redirect('../adminbsm/home/viewanalisa'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Update Analisa Gagal !!</div></div>");
                redirect('../adminbsm/home/viewanalisa'); //jika gagal maka akan ditampilkan form upload
}
			
        }
		function insertcair2() { 
		   $id = $this->input->post('nocair'); 
		   $tglbayardp2 = $this->input->post('tglcair2');
			$nominaldp2 = $this->input->post('nominalcair2');
			
            $data = array ( 
			
			'tglcair2' => $this->input->post('tglcair2'),
			'nominalcair2' => $this->input->post('nominalcair2'),
			); 
            $this->db->where('nocair',$id); 
            $this->db->update('moncus_cair',$data); 
			if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Pencairan Tahap Kedua Berhasil !!</div></div>");
                redirect('../adminbsm/home/addpencairan'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Pencairan Tahap Kedua Gagal !!</div></div>");
                redirect('../adminbsm/home/addpencarian'); //jika gagal maka akan ditampilkan form upload
}
			
        }
		function insertcair3() { 
		   $id = $this->input->post('nocair'); 
		   $tglbayardp2 = $this->input->post('tglcair3');
			$nominaldp2 = $this->input->post('nominalcair3');
			
            $data = array ( 
			
			'tglcair3' => $this->input->post('tglcair3'),
			'nominalcair3' => $this->input->post('nominalcair3'),
			); 
            $this->db->where('nocair',$id); 
            $this->db->update('moncus_cair',$data); 
			if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Pencairan Tahap Ketiga Berhasil !!</div></div>");
                redirect('../adminbsm/home/addpencairan'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Pencairan Tahap Ketiga Gagal !!</div></div>");
                redirect('../adminbsm/home/addpencarian'); //jika gagal maka akan ditampilkan form upload
}
			
        }
		function proseseditdeveloper() { 
		   $id = $this->input->post('iddeveloper'); 
		   $namadeveloper = $this->input->post('namadeveloper');
			$alamat = $this->input->post('alamat');
			$notelp = $this->input->post('notelp');
			$username = $this->input->post('username');
			
            $data = array(
				  'iddeveloper' =>$this->input->post('iddeveloper'),
				  'namadeveloper' =>$this->input->post('namadeveloper'),
				  'alamat' =>$this->input->post('alamat'),
				  'notelp' =>$this->input->post('notelp'),
				  'username' =>$this->input->post('username')
				  );	  
            $this->db->where('iddeveloper',$id); 
            $this->db->update('moncus_developer',$data); 
			if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Update Data Developer Berhasil !!</div></div>");
                redirect('../adminbsm/home/adddeveloper'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Update Data Developer Gagal !!</div></div>");
                redirect('../adminbsm/home/adddeveloper'); //jika gagal maka akan ditampilkan form upload
}
			
        }
		function proseseditsyarat() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditsyarat(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../adminbsm/home/viewsyarat');
        }
		function prosesedituser() { 
		   
			$id = $this->input->post('username');
			$password = md5($this->input->post('password'));
			$signas = $this->input->post('signas');
            $data = array(
				  'username' =>$this->input->post('username'),
				  'password' =>$password,
				  'signas' =>$this->input->post('signas')
				  );	  
            $this->db->where('username',$id); 
            $this->db->update('moncus_user',$data); 
			if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Update Data User Berhasil !!</div></div>");
                redirect('../adminbsm/home/viewuser'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Update Data User Gagal !!</div></div>");
                redirect('../adminbsm/home/viewuser'); //jika gagal maka akan ditampilkan form upload
}
			
        }
function prosesinsertdp3() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelprosesinsertdp3(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Proses Input Data DP Berhasil
				</div>

			 	');
            redirect('../adminbsm/home/viewdp');
        }
	public function insertshgb(){
        $this->load->library('upload');
        $nmfile = 'shgb_'.$this->input->post('noshgb'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/sertifikat'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotoshgb']['name'])
        {
            if ($this->upload->do_upload('fotoshgb'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'nospr' =>$this->input->post('nospr'),
				  'noshgb' =>$this->input->post('noshgb'),
				  'fotoshgb' =>$gbr['file_name']
				 );

                $this->mupload->get_insertshgb($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data SHGB Berhasil !!</div></div>");
                redirect('../adminbsm/home/addsertifikat'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data SHGB Gagal !!</div></div>");
                redirect('../adminbsm/home/addsertifikat'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
		public function insertkondisi(){
        $this->load->library('upload');
        $nmfile = 'rumah_'.$this->input->post('nospr'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/kondisi'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['kondisi']['name'])
        {
            if ($this->upload->do_upload('kondisi'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'nokondisi' =>$this->input->post('nokondisi'),
				  'nospr' =>$this->input->post('nospr'),
				  'kondisi' =>$gbr['file_name']
				 );

                $this->mupload->get_insertkondisi($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Kondisi Rumah Berhasil !!</div></div>");
                redirect('../adminbsm/home/addkondisi'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Kondisi Rumah Gagal !!</div></div>");
                redirect('../adminbsm/home/addkondisi'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
function prosesinsertimb() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelprosesinsertimb(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Proses Input Data IMB Berhasil
				</div>

			 	');
            redirect('../adminbsm/home/viewsertifikat');
        }	

}

/* End of file upload.php */
/* Location: ./application/controllers/upload.php */
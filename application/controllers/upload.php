<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller
{
    var $limit=10;
	var $offset=10;

    public function __construct() {
        parent::__construct();
        $this->load->model('mupload'); //load model mupload yang berada di folder model
        $this->load->helper(array('url')); //load helper url 
		

    }
	function update_image()
{
$nmfile = 'datakurang_'.$this->input->post('noktp');
$config['upload_path'] = './assets/archive/datakurang/';
$config['allowed_types'] = '*';
$config['max_size'] = '1000';
$config['max_width'] = '1024';
$config['max_height'] = '768';
$config['file_name'] = $nmfile;

$this->load->library('upload', $config);
$this->upload->initialize($config);

$error = array();
$gambar = array();
foreach($_FILES as $field_name => $file)
{
if ( ! $this->upload->do_upload($field_name))
{
$error[] = $this->upload->display_errors();
}
else
{ 
$gambar0 = $this->upload->data();
$gambar[] = $gambar0['file_name'];
$data = array(
'noktp' =>$this->input->post('noktp'),
'dk1'=>$gambar[0],
'dk2'=>$gambar[1],
'dk3'=>$gambar[2]
);
}
}
$id=1;
$this->mupload->get_insertkurang1($data);
//akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Kurang Berhasil !!</div></div>");
                redirect('../developer/home/viewspr'); //jika berhasil maka akan ditampilkan view vupload
/*$hasil = $this->mupload->update_image($id,$gambar,$gambar,$gambar);*/
}
function update_image2()
{

$nmfile = 'datakurang_'.$this->input->post('noktp');
$config['upload_path'] = './assets/archive/datakurang/';
$config['allowed_types'] = '*';
$config['max_size'] = '1000';
$config['max_width'] = '1024';
$config['max_height'] = '768';
$config['file_name'] = $nmfile;

$this->load->library('upload', $config);
$this->upload->initialize($config);

$error = array();
$gambar = array();
foreach($_FILES as $field_name => $file)
{
if ( ! $this->upload->do_upload($field_name))
{
$error[] = $this->upload->display_errors();
}
else
{ 
$gambar0 = $this->upload->data();
$gambar[] = $gambar0['file_name'];
}
}
$no=$this->input->post('id');
$hasil=$this->mupload->update_image($no,$gambar,$gambar,$gambar);
//akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Kurang Berhasil !!</div></div>");
                redirect('../developer/home/viewspr'); //jika berhasil maka akan ditampilkan view vupload
/*$hasil = $this->mupload->update_image($id,$gambar,$gambar,$gambar);*/
}
public function insert(){
        $this->load->library('upload');
        $nmfile = $this->input->post('noktp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/uploads/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotodiri']['name'])
        {
            if ($this->upload->do_upload('fotodiri'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'noktp' =>$this->input->post('noktp'),
				  'namanasabah' =>$this->input->post('namanasabah'),
				  'jeniskel' =>$this->input->post('jeniskel'),
				  'tmptlahir' =>$this->input->post('tmptlahir'),
				  'tgllahir' =>$this->input->post('tgllahir'),
				  'alamat' =>$this->input->post('alamat'),
				  'notelp' =>$this->input->post('notelp'),
                  'foto' =>$gbr['file_name'],
				  'iddeveloper' =>$this->session->userdata('iddeveloper')
                );

                $this->mupload->get_insert($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Nasabah Berhasil !!</div></div>");
                redirect('../developer/home/addnas'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Nasabah Berhasil</div></div>");
                redirect('../developer/home/addnas'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
	public function insertpricelist(){
        $this->load->library('upload');
        $nmfile = 'pricelist_'.date(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/pricelist'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['pricelist']['name'])
        {
            if ($this->upload->do_upload('pricelist'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'pricelist' =>$gbr['file_name']
                );

                $this->mupload->get_insertpricelist($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Price List Berhasil !!</div></div>");
                redirect('../developer/home/pricelist'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Price List Gagal !!</div></div>");
                redirect('../developer/home/pricelist'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
	public function insertspr(){
        $this->load->library('upload');
        $nmfile = 'spr_'.$this->input->post('nospr'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/spr'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotospr']['name'])
        {
            if ($this->upload->do_upload('fotospr'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'nospr' =>$this->input->post('nospr'),
				  'noktp' =>$this->input->post('noktp'),
				  'alamatobjek' =>$this->input->post('alamatobjek'),
				  'hargabangunan' =>$this->input->post('hargabangunan'),
				  'plafonbiaya' =>$this->input->post('plafonbiaya'),
				  'jangkawkt' =>$this->input->post('jangkawkt'),
                  'fotospr' =>$gbr['file_name'],
				  'iddeveloper' =>$this->session->userdata('iddeveloper'),
				  'marketingdeveloper' =>$this->input->post('marketingdeveloper')
                );

                $this->mupload->get_insertspr($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data SPR Berhasil !!</div></div>");
                redirect('../developer/home/addspr'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data SPR Gagal !!</div></div>");
                redirect('../developer/home/addspr'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
	public function insertdatakurang(){
        $this->load->library('upload');
        $nmfile = 'dk_'.$this->input->post('noktp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/datakurang'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
         for ($i = 1; $i <=4; $i++){
        if($_FILES['dk1']['name'])
        {
            if ($this->upload->do_upload('dk1'))
            {

                $gbr = $this->upload->data();
                $data = array(
				  'noktp' =>$this->input->post('noktp'),
				  'dk1' =>$gbr['file_name'],
				  'dk2' =>$gbr['file_name'],
				  'dk3' =>$gbr['file_name']
				  );
				$this->mupload->get_insertkurang1($data); //akses model untuk menyimpan ke database
				//pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Kurang Berhasil Dilengkapi !!</div></div>");
                redirect('../developer/home/viewspr'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Kurang Gagal Dilengkapi !!</div></div>");
                redirect('../developer/home/viewspr'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
	}
	
	public function insertdp1(){
        $this->load->library('upload');
        $nmfile = 'dp1_'.time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/kwitansi/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotokwitansi1']['name'])
        {
            if ($this->upload->do_upload('fotokwitansi1'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'nodp' =>$this->input->post('nodp'),
				  'nospr' =>$this->input->post('nospr'),
				  'tglbayardp1' =>$this->input->post('tglbayardp1'),
				  'fotokwitansi1' => $gbr['file_name'],
				  'nominaldp1' =>$this->input->post('nominaldp1')
				  );
				  $this->mupload->get_insertdp($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data DP Berhasil !!</div></div>");
                redirect('../developer/home/adddp'); //jika berhasil maka akan ditampilkan view vupload
            $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data DP Berhasil !!</div></div>");
                redirect('../developer/home/adddp'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data DP Gagal !!</div></div>");
                redirect('../developer/home/adddp'); //jika gagal maka akan ditampilkan form upload
}
        }
		         
    }
	public function editdeveloper($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('mupload');
		$data['data']=$this->mupload->get_edit($id);
		$this->load->view('developer/feditnas', $data);
        
    }
	public function editprofil($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('mupload');
		$data['data']=$this->mupload->get_edit($id);
		$this->load->view('developer/feditnas', $data);
        
    }
	function proseseditprofil() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditprofil(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../developer/home/viewnas');
        }
	function proseseditspr() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditspr(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../developer/home/viewspr');
        }
		function proseseditkondisi() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditkondisi(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../developer/home/viewkondisi');
        }
		function proseseditpricelist() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditpricelist(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../developer/home/viewpricelist');
        }
		function proseseditshgb() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditshgb(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../developer/home/viewsertifikat');
        }
		function proseseditdp() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelproseseditdp(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../developer/home/viewdp');
        }
		function insertdp2() { 
		   $this->load->model('mupload','',TRUE); 
            $this->mupload->modelprosesinsertdp2(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Proses Input Data DP Tahap Kedua Berhasil
				</div>

			 	');
            redirect('../developer/home/viewdp');
		   
		}
function prosesinsertdp3() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelprosesinsertdp3(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Proses Input Data DP Berhasil
				</div>

			 	');
            redirect('../developer/home/adddp');
        }
	public function insertshgb(){
        $this->load->library('upload');
        $nmfile = 'shgb_'.$this->input->post('noshgb'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/sertifikat'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotoshgb']['name'])
        {
            if ($this->upload->do_upload('fotoshgb'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'nospr' =>$this->input->post('nospr'),
				  'noshgb' =>$this->input->post('noshgb'),
				  'fotoshgb' =>$gbr['file_name']
				 );

                $this->mupload->get_insertshgb($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data SHGB Berhasil !!</div></div>");
                redirect('../developer/home/addsertifikat'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data SHGB Gagal !!</div></div>");
                redirect('../developer/home/addsertifikat'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
		public function insertkondisi(){
        $this->load->library('upload');
        $nmfile = 'rumah_'.$this->input->post('nospr'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/kondisi'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['kondisi']['name'])
        {
            if ($this->upload->do_upload('kondisi'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'nokondisi' =>$this->input->post('nokondisi'),
				  'nospr' =>$this->input->post('nospr'),
				  'kondisi' =>$gbr['file_name']
				 );

                $this->mupload->get_insertkondisi($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Kondisi Rumah Berhasil !!</div></div>");
                redirect('../developer/home/addkondisi'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Kondisi Rumah Gagal !!</div></div>");
                redirect('../developer/home/addkondisi'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
	public function insertsimulasi(){
        $this->load->library('upload');
        $nmfile = 'simulasi'.date('d-m-y'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/simulasi'; //path folder
        $config['allowed_types'] = 'xls|xlsx|odt'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['simulasiangsuran']['name'])
        {
            if ($this->upload->do_upload('simulasiangsuran'))
            {
                $gbr = $this->upload->data();
                $data = array(
				  'simulasiangsuran' =>$gbr['file_name']
				  );

                $this->mupload->get_insertsimulasi($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data File Simulasi Berhasil !!</div></div>");
                redirect('../developer/home/addsimulasi'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Simulasi Gagal !!</div></div>");
                redirect('../developer/home/addsimulasi'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
function prosesinsertimb() { 
		$this->load->model('mupload','',TRUE); 
            $this->mupload->modelprosesinsertimb(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Proses Input Data IMB Berhasil
				</div>

			 	');
            redirect('../developer/home/viewsertifikat');
        }	

}

/* End of file upload.php */
/* Location: ./application/controllers/upload.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	// public function index() {
		// $this->load->view('index');
	// }

	public function index($error = NULL) {
        $data = array(
            'title' => 'Login Page',
            'action' => site_url('auth/login'),
            'error' => $error
        );
        $this->load->view('login', $data);
    }
	
	public function cek_login() {
		$data = array('username' => $this->input->post('username', TRUE),
						'pass' => md5($this->input->post('password', TRUE))
			);
		$this->load->model('model_user'); // load model_user
		$hasil = $this->model_user->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['uid'] = $sess->uid;
				$sess_data['noktp'] = $sess->noktp;
				$sess_data['username'] = $sess->username;
				$sess_data['level'] = $sess->level;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('level')=='member') {
				redirect('../home/memberpage');
			}		
		}
		else {
			$this->load->view('login/failed_login');
		}
	}
	
	public function cek_loginadmin() {
		$data = array('username' => $this->input->post('username', TRUE),
						'password' => md5($this->input->post('password', TRUE)));
		$data1 = array('username' => $this->input->post('username', TRUE));
		$databsm = array('username' => $this->input->post('username', TRUE));
		$this->load->model('vmodul_admin'); // load model_user
		$hasil = $this->vmodul_admin->cek_user($data);
		$masukviewuser = $this->vmodul_admin->cantumuser($data1);
		$masukviewuserbsm = $this->vmodul_admin->cantumuserbsm($databsm);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['uid'] = $sess->uid;
				$sess_data['username'] = $sess->username;
				$sess_data['signas'] = $sess->signas;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('signas')=='Super Admin') {
				redirect('../admin/home/adminpage');
			}
			elseif ($this->session->userdata('signas')=='Admin Developer') {
				foreach ($masukviewuser->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['uid'] = $sess->uid;
				$sess_data['username'] = $sess->username;
				$sess_data['iddeveloper'] = $sess->iddeveloper;
				$sess_data['namadeveloper'] = $sess->namadeveloper;
				$sess_data['signas'] = $sess->signas;
				$this->session->set_userdata($sess_data);
			}
				redirect('../developer/home/developerpage');
			}
			elseif ($this->session->userdata('signas')=='Admin BSM') {
				foreach ($masukviewuserbsm->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['uid'] = $sess->uid;
				$sess_data['username'] = $sess->username;
				$sess_data['iddeveloper'] = $sess->iddeveloper;
				$sess_data['namadeveloper'] = $sess->namadeveloper;
				$sess_data['signas'] = $sess->signas;
				$this->session->set_userdata($sess_data);
			}
				redirect('../adminbsm/home/adminpage');
			}
			elseif ($this->session->userdata('signas')=='Supervisor Admin BSM') {
				foreach ($masukviewuserbsm->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['uid'] = $sess->uid;
				$sess_data['username'] = $sess->username;
				$sess_data['iddeveloper'] = $sess->iddeveloper;
				$sess_data['namadeveloper'] = $sess->namadeveloper;
				$sess_data['signas'] = $sess->signas;
				$this->session->set_userdata($sess_data);
			}
				redirect('../supervisorbsm/home/supervisorpage');
			}
			elseif ($this->session->userdata('signas')=='Supervisor Admin Developer') {
				foreach ($masukviewuserbsm->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['uid'] = $sess->uid;
				$sess_data['username'] = $sess->username;
				$sess_data['iddeveloper'] = $sess->iddeveloper;
				$sess_data['namadeveloper'] = $sess->namadeveloper;
				$sess_data['signas'] = $sess->signas;
				$this->session->set_userdata($sess_data);
			}
				redirect('../supervisordeveloper/home/supervisorpage');
			}
		}
		else {
			$this->load->view('login/failed_login');
		}
	}

}

?>

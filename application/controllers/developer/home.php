<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','download'));
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function index()
	{
		$this->load->view('developer/home');
	}
	public function developerpage()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('developer/index',$data);
	}
	public function viewcollect()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatacollect();
		$this->load->view('developer/ftampilcollect',$data);
	}
	public function viewsimulasi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampilsimulasiangsuran();
		$this->load->view('developer/ftampilsimulasi',$data);
	}
	public function viewpencairan()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatapencairan();
		$this->load->view('developer/ftampilpencairan',$data);
	}
	public function viewanalisa()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildataanalisa();
		$this->load->view('developer/ftampilanalisa',$data);
	}
	public function viewsp3()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasp3();
		$this->load->view('developer/ftampilsppp',$data);
	}
	public function viewakad()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildataakad();
		$this->load->view('developer/ftampilakad',$data);
	}
	public function addnas()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('developer/finputnas',$data);
	}
	public function addsimulasi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('developer/finputsimulasi',$data);
	}
	public function addspr()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['no_spr']=$this->vmodul_admin->get_nospr();
		$data['tampildatadeveloper']=$this->vmodul_admin->getnamadeveloper();
		$data['tampildatanasabah']=$this->vmodul_admin->getnamanasabah();
		$this->load->view('developer/finputspr',$data);
	}
	public function adddp()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['no_dp']=$this->vmodul_admin->get_nodp();
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['tampildatadp']=$this->vmodul_admin->getdp();
		$this->load->view('developer/finputdp',$data);
	}
	public function addsertifikat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('developer/finputsertifikat',$data);
	}
	public function addkondisi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['no_kondisi']=$this->vmodul_admin->get_nokondisi();
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('developer/finputkondisi',$data);
	}
	public function viewnas()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatanasabah();
		$this->load->view('developer/ftampilnas',$data);
	}
	public function viewdatakurang()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatakurang();
		$this->load->view('developer/ftampilspr',$data);
	}
	public function viewspr()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildataspr();
		$data['datakurang']=$this->vmodul_admin->tampildatakurang();
		$data['tampildatanasabah']=$this->vmodul_admin->getnamanasabah();
		$this->load->view('developer/ftampilspr',$data);
	}
	public function pricelist()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$this->load->view('developer/finputpricelist',$data);
	}
	public function viewsyarat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasyarat();
		$this->load->view('developer/ftampilsyarat',$data);
	}
	public function viewpricelist()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatapricelist();
		$this->load->view('developer/ftampilpricelist',$data);
	}
	public function viewdp()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatadp();
		$this->load->view('developer/ftampildp',$data);
	}
	public function viewsertifikat()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatasertifikat();
		$this->load->view('developer/ftampilsertifikat',$data);
	}
	public function viewkondisi()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->tampildatakondisi();
		$this->load->view('developer/ftampilkondisi',$data);
	}
	public function deletenas($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_nasabah($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../developer/home/viewnas');
            }
		$this->load->view('developer/ftampilnas', $data);
	}
	public function deletekondisi($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_kondisi($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../developer/home/viewkondisi');
            }
		$this->load->view('developer/ftampilnas', $data);
	}
	public function deletespr($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_dpspr($id);
		$data['data']=$this->vmodul_admin->hapus_sertifikat($id);
		$data['data']=$this->vmodul_admin->hapus_analisaspr($id);
		$data['data']=$this->vmodul_admin->hapus_berkasspr($id);
		$data['data']=$this->vmodul_admin->hapus_kondisispr($id);
		$data['data']=$this->vmodul_admin->hapus_spr($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data SPR Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../developer/home/viewspr');
            }
		$this->load->view('developer/ftampilspr', $data);
	}
	public function deletedp($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('vmodul_admin','',TRUE); 
		$data['data']=$this->vmodul_admin->hapus_dp($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data DP Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../developer/home/viewdp');
            }
		$this->load->view('developer/ftampildp', $data);
	}
	public function editspr($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editspr($id);
		$data['tampildatanasabah']=$this->vmodul_admin->getnamanasabah();
		$this->load->view('developer/feditspr', $data);
        
    }
	public function editpricelist($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editpricelist($id);
		$this->load->view('developer/feditpricelist', $data);
        
    }
	public function editsertifikat($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editsertifikat($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('developer/feditsertifikat', $data);
        
    }
	public function editimb($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editimb($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('developer/feditimb', $data);
        
    }
	public function editdp($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editdp($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$data['tampildatadp']=$this->vmodul_admin->getdp();
		$this->load->view('developer/feditdp', $data);
        
    }
	public function editkondisi($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editkondisi($id);
		$data['tampildataspr']=$this->vmodul_admin->getspr();
		$this->load->view('developer/feditkondisi', $data);
        
    }
	public function editdatakurang($id){
	 $data['username'] = $this->session->userdata('username');
		$this->load ->model('vmodul_admin');
		$data['data']=$this->vmodul_admin->get_editdatakurang($id);
		$data['tampildatanasabah']=$this->vmodul_admin->getnamanasabah();
		$this->load->view('developer/feditdatakurang', $data);
        
    }
	public function formupload()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('developer/fupload',$data);
	}
	 
	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('');
	}

}

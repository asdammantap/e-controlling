<?php 
	Class vmodul_admin extends CI_Model {

		Function cek_user($data) {
			$query = $this->db->get_where('moncus_user', $data);
			return $query;
		}
		Function cantumuser($data1) {
			$query = $this->db->get_where('view_userdeveloper', $data1);
			return $query;
		}
		Function cantumuserbsm($databsm) {
			$query = $this->db->get_where('view_userbsm', $databsm);
			return $query;
		}
		public function get_idanalisa() {
  $tahun = date("m-Y");
  $kode = 'ANALYST';
  $query = $this->db->query("SELECT MAX(idanalisa) as autoidanalisa FROM moncus_analis"); 
  $row = $query->row_array();
  $autoid_analisa = $row['autoidanalisa']; 
  $autoid_analisa1 =(int) substr($autoid_analisa,9,5);
  $id_analisa = $autoid_analisa1 +1;
  $autoidanalisa = $kode.sprintf("%04s",$id_analisa).$tahun;
  return $autoidanalisa;
 }
 public function get_noberkas() {
  $tahun = date("m-Y");
  $kode = 'CDATA';
  $query = $this->db->query("SELECT MAX(noberkas) as autonoberkas FROM moncus_berkas"); 
  $row = $query->row_array();
  $autono_berkas = $row['autonoberkas']; 
  $autono_berkas1 =(int) substr($autono_berkas,9,5);
  $no_berkas = $autono_berkas1 +1;
  $autonoberkas = $kode.sprintf("%04s",$no_berkas).$tahun;
  return $autonoberkas;
 }
 public function get_nodp() {
  $tahun = date("d-m-Y");
  $kode = 'DP';
  $query = $this->db->query("SELECT MAX(nodp) as autonodp FROM moncus_dp"); 
  $row = $query->row_array();
  $autono_dp = $row['autonodp']; 
  $autono_dp1 =(int) substr($autono_dp,9,5);
  $no_dp = $autono_dp1 +1;
  $autonodp = $kode.sprintf("%04s",$no_dp).$tahun;
  return $autonodp;
 }
 public function get_nokondisi() {
  $tahun = date("d-m-Y");
  $kode = 'KRUM';
  $query = $this->db->query("SELECT MAX(nokondisi) as autonokondisi FROM moncus_kondisi"); 
  $row = $query->row_array();
  $autono_kondisi = $row['autonokondisi']; 
  $autono_kondisi1 =(int) substr($autono_kondisi,9,5);
  $no_kondisi = $autono_kondisi1 +1;
  $autonokondisi = $kode.sprintf("%04s",$no_kondisi).$tahun;
  return $autonokondisi;
 }
 public function get_nospr() {
  $tahun = date("m-Y");
  $kode = 'SPR';
  $query = $this->db->query("SELECT MAX(nospr) as autonospr FROM moncus_spr"); 
  $row = $query->row_array();
  $autono_spr = $row['autonospr']; 
  $autono_spr =(int) substr($autono_spr,9,5);
  $no_spr = $autono_spr +1;
  $autonospr = $kode.sprintf("%04s",$no_spr).$tahun;
  return $autonospr;
 }
 public function get_cair() {
  $tahun = date("m-Y");
  $kode = 'PD';
  $query = $this->db->query("SELECT MAX(nocair) as autonocair FROM moncus_cair"); 
  $row = $query->row_array();
  $autono_cair = $row['autonocair']; 
  $autono_cair =(int) substr($autono_cair,9,5);
  $no_cair = $autono_cair +1;
  $autonocair = $kode.sprintf("%04s",$no_cair).$tahun;
  return $autonocair;
 }
 public function get_akad() {
  $tahun = date("d-m-Y");
  $kode = 'AKAD';
  $query = $this->db->query("SELECT MAX(noakad) as autonoakad FROM moncus_akad"); 
  $row = $query->row_array();
  $autono_akad = $row['autonoakad']; 
  $autono_akad =(int) substr($autono_akad,9,5);
  $no_akad = $autono_akad +1;
  $autonoakad = $kode.sprintf("%04s",$no_akad).$tahun;
  return $autonoakad;
 }
		Function tampildatanasabah()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('moncus_nasabah');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
		Function tampillaporan()
	{
		$query=$this->db->get('view_reportall');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampilsimulasiangsuran()
	{
		$query=$this->db->get('moncus_simulasi');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatakurang()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_datakurang');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
		Function tampildatadeveloper()
	{
		$query=$this->db->get('view_developer');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatasyarat()
	{
		$this->db->where('no = 1');
		$query=$this->db->get('moncus_sypls');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatapricelist()
	{
		$this->db->where('no = 1');
		$query=$this->db->get('moncus_pl');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatauser()
	{
		$query=$this->db->get('moncus_user');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatauserbsm()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_userdeveloper');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
		Function tampildatacollect()
	{
		$query=$this->db->get('view_collectdata');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatacollectbsm()
	{
		
		$query=$this->db->get('view_collectdata');
		If ($query->num_rows()>0)
	{
		Return $query->result();
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
	}
		Else
	{
		Return array();
	}
	}	
		Function tampildataanalisa()
	{
		$query=$this->db->get('view_analisa');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildataanalisabsm()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_analisa');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildataspr()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_spr');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatasprbsm()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_spr');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatadp()
	{
		$query=$this->db->get('view_dp');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	
	Function tampildatadpbsm()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_dp');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}	
	Function tampildatapencairan()
	{
		$query=$this->db->get('view_pencairan');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatapencairanbsm()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_pencairan');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatasp3()
	{
		$query=$this->db->get('view_sp3');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatasp3bsm()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_sp3');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatasertifikat()
	{
		$query=$this->db->get('view_sertifikatimb');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatasertifikatbsm()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_sertifikatimb');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}	
	Function tampildatakondisi()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_kondisibangunan');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildatakondisibsm()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_kondisibangunan');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}	
	Function tampildataakad()
	{
		$query=$this->db->get('view_akad');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampildataakadbsm()
	{
		$this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query=$this->db->get('view_akad');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	public function hapus_nasabah($id){ 
			
			$this->db->where('noktp',$id);
			$query = $this->db->get('moncus_nasabah');
			$row = $query->row();

			unlink("./assets/uploads/$row->foto");

			$this->db->delete('moncus_nasabah', array('noktp' => $id));

}
public function hapus_nasabahdeveloper($id){ 
			
			$this->db->where('iddeveloper',$id);
			$query = $this->db->get('view_userdeveloper');
			$row = $query->row();

			$this->db->delete('moncus_nasabah', array('iddeveloper' => $id));

}
public function hapus_user($id){ 
			
			$this->db->where('username',$id);
			$query = $this->db->get('moncus_user');
			$row = $query->row();
			$this->db->delete('moncus_user', array('username' => $id));

}
public function hapus_userbsm($id){ 
			
			$this->db->where('username',$id);
			$query = $this->db->get('moncus_userbsm');
			$row = $query->row();
			$this->db->delete('moncus_userbsm', array('username' => $id));

}
public function hapus_developer($id){ 
			
			$this->db->where('iddeveloper',$id);
			$query = $this->db->get('moncus_developer');
			$row = $query->row();
			$this->db->delete('moncus_developer', array('iddeveloper' => $id));

}
public function hapus_sppp($id){ 
			
			$this->db->where('nosppp',$id);
			$query = $this->db->get('moncus_sppp');
			$row = $query->row();
			unlink("./assets/archive/sppp/$row->fotosppp");
			$this->db->delete('moncus_sppp', array('nosppp' => $id));

}
public function hapus_spppanalisa($id){ 
			
			$this->db->where('idanalisa',$id);
			$query = $this->db->get('moncus_sppp');
			$row = $query->row();
			$this->db->delete('moncus_sppp', array('idanalisa' => $id));

}
public function hapus_akad($id){ 
			
			$this->db->where('noakad',$id);
			$query = $this->db->get('moncus_akad');
			$row = $query->row();
			$this->db->delete('moncus_akad', array('noakad' => $id));

}
public function hapus_akadkondisi($id){ 
			
			$this->db->where('nosppp',$id);
			$query = $this->db->get('moncus_akad');
			$row = $query->row();
			$this->db->delete('moncus_akad', array('nosppp' => $id));

}
public function hapus_akadanalisa($id){ 
			
			$this->db->where('nosppp',$id);
			$query = $this->db->get('moncus_akad');
			$row = $query->row();
			$this->db->delete('moncus_akad', array('nosppp' => $id));

}
public function hapus_cair($id){ 
			
			$this->db->where('nocair',$id);
			$query = $this->db->get('moncus_cair');
			$row = $query->row();
			$this->db->delete('moncus_cair', array('nocair' => $id));

}
public function hapus_cairanalisa($id){ 
			
			$this->db->where('nosppp',$id);
			$query = $this->db->get('moncus_cair');
			$row = $query->row();
			$this->db->delete('moncus_cair', array('nosppp' => $id));

}
public function hapus_analisa($id){ 
			
			$this->db->where('idanalisa',$id);
			$query = $this->db->get('moncus_analis');
			$row = $query->row();
			$this->db->delete('moncus_analis', array('idanalisa' => $id));

}
public function hapus_analisaberkas($id){ 
			
			$this->db->where('noberkas',$id);
			$query = $this->db->get('view_collectdata');
			$row = $query->row();
			$this->db->delete('moncus_analis', array('noberkas' => $id));

}
public function hapus_berkas($id){ 
			
			$this->db->where('noberkas',$id);
			$query = $this->db->get('moncus_berkas');
			$row = $query->row();
			$this->db->delete('moncus_berkas', array('noberkas' => $id));

}
public function hapus_developeruser($id){ 
			
			$this->db->where('username',$id);
			$query = $this->db->get('moncus_developer');
			$row = $query->row();
			$this->db->delete('moncus_developer', array('username' => $id));

}
public function hapus_spradmin($id){ 
			
			$this->db->where('noktp',$id);
			$query = $this->db->get('moncus_spr');
			$row = $query->row();

			unlink("./assets/archive/spr/$row->fotospr");

			$this->db->delete('moncus_spr', array('noktp' => $id));

}
public function hapus_spr($id){ 
			
			$this->db->where('nospr',$id);
			$query = $this->db->get('moncus_spr');
			$row = $query->row();

			unlink("./assets/archive/spr/$row->fotospr");

			$this->db->delete('moncus_spr', array('nospr' => $id));

}
public function hapus_sertifikat($id){ 
			
			$this->db->where('nospr',$id);
			$query = $this->db->get('moncus_sertifikat');
			$row = $query->row();

			unlink("./assets/archive/sertifikat/$row->fotoshgb");
			unlink("./assets/archive/imb/$row->fotoimb");

			$this->db->delete('moncus_sertifikat', array('nospr' => $id));

}
public function hapus_berkasspr($id){ 
			
			$this->db->where('nospr',$id);
			$query = $this->db->get('moncus_berkas');
			$row = $query->row();
			$this->db->delete('moncus_berkas', array('nospr' => $id));

}
public function hapus_analisaspr($id){ 
			
			$this->db->where('nospr',$id);
			$query = $this->db->get('moncus_analis');
			$row = $query->row();
			$this->db->delete('moncus_analis', array('nospr' => $id));

}
public function hapus_dpspr($id){ 
			
			$this->db->where('nospr',$id);
			$query = $this->db->get('moncus_dp');
			$row = $query->row();

			unlink("./assets/archive/kwitansi/$row->fotokwitansi");

			$this->db->delete('moncus_dp', array('nospr' => $id));

}
public function hapus_dp($id){ 
			
			$this->db->where('nodp',$id);
			$query = $this->db->get('moncus_dp');
			$row = $query->row();

			unlink("./assets/archive/kwitansi/$row->fotokwitansi");

			$this->db->delete('moncus_dp', array('nodp' => $id));

}
public function hapus_kondisispr($id){ 
			
			$this->db->where('nospr',$id);
			$query = $this->db->get('moncus_kondisi');
			$row = $query->row();

			unlink("./assets/archive/kondisi/$row->kondisi");

			$this->db->delete('moncus_kondisi', array('nospr' => $id));

}
public function hapus_kondisi($id){ 
			
			$this->db->where('nokondisi',$id);
			$query = $this->db->get('moncus_kondisi');
			$row = $query->row();

			unlink("./assets/archive/kondisi/$row->kondisi");

			$this->db->delete('moncus_kondisi', array('nokondisi' => $id));

}
public function get_editspr($id) { 
                $this->db->where('nospr',$id); 
                $query = $this->db->get('view_spr'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editdatakurang($id) { 
                $this->db->where('noktp',$id); 
                $query = $this->db->get('view_datakurang'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editcair($id) { 
                $this->db->where('nocair',$id); 
                $query = $this->db->get('moncus_cair'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editakad($id) { 
                $this->db->where('noakad',$id); 
                $query = $this->db->get('moncus_akad'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editsyarat($id) { 
                $this->db->where('no',$id); 
                $query = $this->db->get('moncus_sypls'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editcollect($id) { 
                $this->db->where('noberkas',$id); 
                $query = $this->db->get('moncus_berkas'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editsppp($id) { 
                $this->db->where('nosppp',$id); 
                $query = $this->db->get('moncus_sppp'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editanalisa($id) { 
                $this->db->where('idanalisa',$id); 
                $query = $this->db->get('moncus_analis'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editpricelist($id) { 
                $this->db->where('no',$id); 
                $query = $this->db->get('moncus_pl'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editdeveloper($id) { 
                $this->db->where('iddeveloper',$id); 
                $query = $this->db->get('view_userdeveloper'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_edituser($id) { 
                $this->db->where('username',$id); 
                $query = $this->db->get('moncus_user'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editdp($id) { 
                $this->db->where('nodp',$id); 
                $query = $this->db->get('view_dp'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
public function get_editkondisi($id) { 
                $this->db->where('nokondisi',$id); 
                $query = $this->db->get('view_kondisibangunan'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editsertifikat($id) { 
                $this->db->where('noshgb',$id); 
                $query = $this->db->get('view_sertifikatimb'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_detailsertifikat($id) { 
                $this->db->where('nospr',$id); 
                $query = $this->db->get('view_sertifikatimb'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editimb($id) { 
                $this->db->where('noimb',$id); 
                $query = $this->db->get('view_sertifikatimb'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }

function getnamanasabah()
 {
  $this->db->where('iddeveloper',$this->session->userdata('iddeveloper'));
		$query = $this->db->query("SELECT * FROM moncus_nasabah ");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['noktp']] = $row['namanasabah'];
 }
 }
 return $data;
 }
 function getnamadeveloper()
 {
 $query = $this->db->query("SELECT * FROM moncus_developer");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['iddeveloper']] = $row['namadeveloper'];
 }
 }
 return $data;
 }
 function getspr()
 {
 $query = $this->db->query("SELECT * FROM moncus_spr");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['nospr']] = $row['nospr'];
 }
 }
 return $data;
 }
 
 function getnosp3()
 {
 $query = $this->db->query("SELECT * FROM view_sp3");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['nosppp']] = $row['nosppp'];
 }
 }
 return $data;
 }
 function getnokondisi()
 {
 $query = $this->db->query("SELECT * FROM view_kondisibangunan");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['nokondisi']] = $row['nospr'];
 }
 }
 return $data;
 }
 function getnocair()
 {
 $query = $this->db->query("SELECT * FROM moncus_cair");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['nocair']] = $row['nocair'];
 }
 }
 return $data;
 }
 
 function getberkas()
 {
 $query = $this->db->query("SELECT * FROM moncus_berkas");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['noberkas']] = $row['noberkas'];
 }
 }
 return $data;
 }
 function getidanalisa()
 {
 $query = $this->db->query("SELECT * FROM moncus_analis");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['idanalisa']] = $row['idanalisa'];
 }
 }
 return $data;
 }
 function getnoberkas()
 {
 $query = $this->db->query("SELECT * FROM moncus_berkas");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['noberkas']] = $row['noberkas'];
 }
 }
 return $data;
 }
 /*$this->session->userdata('iddeveloper')*/
  function getdp()
 {
 
 $query = $this->db->query("SELECT * FROM view_dp");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['nodp']] = $row['nospr'];
 }
 }
 return $data;
 }
}
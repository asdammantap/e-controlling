<?php
class Mupload extends CI_Model {

    var $tabel = 'moncus_nasabah';
	var $tabel1 = 'moncus_spr';
	var $tabel2 = 'moncus_dp';
	var $tabel3 = 'moncus_sertifikat';
	var $tabel4 = 'moncus_kondisi';
	var $tabellogin = 'moncus_user';
	var $tabelloginbsm = 'moncus_userbsm';
	var $tabelloginspv = 'moncus_spvdeveloper';
	var $tabeldev = 'moncus_developer';
	var $tabelcollect = 'moncus_berkas';
	var $tabelanalisa = 'moncus_analis';
	var $tabelsp3 = 'moncus_sppp';
	var $tabelcair = 'moncus_cair';
	var $tabelakad = 'moncus_akad';
	var $tabelsyarat = 'moncus_sypls';
	var $tabelpricelist = 'moncus_pl';
	var $tabeldatakurang = 'moncus_datakurang';
	var $tabeldatasimulasi = 'moncus_simulasi';
	
    function __construct() {
        parent::__construct();
    }
    
	function update_image($no,$gambar,$gambar,$gambar)
{
$data = array(
'dk1'=>$gambar[0],
'dk2'=>$gambar[1],
'dk3'=>$gambar[2]
);
$this->db->where('id',$no);
return $this->db->update('moncus_datakurang',$data);
}

    //fungsi insert ke database
    function get_insert($data){
       $this->db->insert($this->tabel, $data);
       return TRUE;
    }
	function get_insertkurang1($data){
       $this->db->insert($this->tabeldatakurang, $data);
       return TRUE;
    }
	function get_insertsimulasi($data){
       $this->db->insert($this->tabeldatasimulasi, $data);
       return TRUE;
    }
	public function get_edit($id) { 
                $this->db->where('noktp',$id); 
                $query = $this->db->get('moncus_nasabah'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editadmin($id) { 
                $this->db->where('noktp',$id); 
                $query = $this->db->get('view_nasabah'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_editdeveloper($id) { 
                $this->db->where('iddeveloper',$id); 
                $query = $this->db->get('moncus_developer'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
			
        }
		function modelproseseditprofiladmin() { 
		
			$id = $this->input->post('noktp'); 
			$this->load->library('upload');
        $nmfile = 'file_'.$this->input->post('noktp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/uploads/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '1288'; //lebar maksimum 1288 px
        $config['max_height']  = '768'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotodiri']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('fotodiri'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'noktp' =>$this->input->post('noktp'),
				  'namanasabah' =>$this->input->post('namanasabah'),
				  'jeniskel' =>$this->input->post('jeniskel'),
				  'tmptlahir' =>$this->input->post('tmptlahir'),
				  'tgllahir' =>$this->input->post('tgllahir'),
				  'alamat' =>$this->input->post('alamat'),
				  'notelp' =>$this->input->post('notelp'),
                  'foto' =>$gbr['file_name'],
				  'iddeveloper' =>$this->input->post('iddeveloper')
				  );
				$this->db->where('noktp',$id); 
            $this->db->update('moncus_nasabah',$data); 
			}
		}
		else{
		$data = array(
				  'noktp' =>$this->input->post('noktp'),
				  'namanasabah' =>$this->input->post('namanasabah'),
				  'jeniskel' =>$this->input->post('jeniskel'),
				  'tmptlahir' =>$this->input->post('tmptlahir'),
				  'tgllahir' =>$this->input->post('tgllahir'),
				  'alamat' =>$this->input->post('alamat'),
				  'notelp' =>$this->input->post('notelp'),
				  'iddeveloper' =>$this->input->post('iddeveloper')
				  );
				$this->db->where('noktp',$id); 
            $this->db->update('moncus_nasabah',$data); 
		}
               
        }
		function modelproseseditprofil() { 
		
			$id = $this->input->post('noktp'); 
			$this->load->library('upload');
        $nmfile = 'file_'.$this->input->post('noktp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/uploads/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '1288'; //lebar maksimum 1288 px
        $config['max_height']  = '768'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotodiri']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('fotodiri'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'noktp' =>$this->input->post('noktp'),
				  'namanasabah' =>$this->input->post('namanasabah'),
				  'jeniskel' =>$this->input->post('jeniskel'),
				  'tmptlahir' =>$this->input->post('tmptlahir'),
				  'tgllahir' =>$this->input->post('tgllahir'),
				  'alamat' =>$this->input->post('alamat'),
				  'notelp' =>$this->input->post('notelp'),
                  'foto' =>$gbr['file_name']
                );
				$this->db->where('noktp',$id); 
            $this->db->update('moncus_nasabah',$data); 
			}
		}
		else{
		$data = array(
				  'noktp' =>$this->input->post('noktp'),
				  'namanasabah' =>$this->input->post('namanasabah'),
				  'jeniskel' =>$this->input->post('jeniskel'),
				  'tmptlahir' =>$this->input->post('tmptlahir'),
				  'tgllahir' =>$this->input->post('tgllahir'),
				  'alamat' =>$this->input->post('alamat'),
				  'notelp' =>$this->input->post('notelp')
                  );
				$this->db->where('noktp',$id); 
            $this->db->update('moncus_nasabah',$data); 
		}
               
        }
		
	 //fungsi insert ke database
    function get_insertspr($data){
       $this->db->insert($this->tabel1, $data);
       return TRUE;
    }
	 function get_insertdp($data){
       $this->db->insert($this->tabel2, $data);
       return TRUE;
    }
	function get_insertcair($data){
       $this->db->insert($this->tabelcair, $data);
       return TRUE;
    }
	function get_insertakad($data){
       $this->db->insert($this->tabelakad, $data);
       return TRUE;
    }
	 function get_insertshgb($data){
       $this->db->insert($this->tabel3, $data);
       return TRUE;
    }
	 function get_insertlogin($data){
       $this->db->insert($this->tabellogin, $data);
       return TRUE;
    }
	 function get_insertloginuserbsm($databsm){
       $this->db->insert($this->tabelloginbsm, $databsm);
       return TRUE;
    }
	function get_insertloginspv($dataspv){
       $this->db->insert($this->tabelloginspv, $dataspv);
       return TRUE;
    }
	function get_insertsyarat($data){
       $this->db->insert($this->tabelsyarat, $data);
       return TRUE;
    }
	 function get_insertdeveloper($data){
       $this->db->insert($this->tabeldev, $data);
       return TRUE;
    }
	function get_insertkondisi($data){
       $this->db->insert($this->tabel4, $data);
       return TRUE;
    }
	function get_insertcollect($data){
       $this->db->insert($this->tabelcollect, $data);
       return TRUE;
    }
	function get_insertanalisa($data){
       $this->db->insert($this->tabelanalisa, $data);
       return TRUE;
    }
	function get_insertpricelist($data){
       $this->db->insert($this->tabelpricelist, $data);
       return TRUE;
    }
	function get_insertsppp($data){
       $this->db->insert($this->tabelsp3, $data);
       return TRUE;
    }
	function modelproseseditspr() { 
		
			$id = $this->input->post('nospr'); 
			$this->load->library('upload');
        $nmfile = 'spr_'.$this->input->post('nospr'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/spr/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotospr']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('fotospr'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'nospr' =>$this->input->post('nospr'),
				  'noktp' =>$this->input->post('noktp'),
				  'alamatobjek' =>$this->input->post('alamatobjek'),
				  'hargabangunan' =>$this->input->post('hargabangunan'),
				  'plafonbiaya' =>$this->input->post('plafonbiaya'),
				  'jangkawkt' =>$this->input->post('jangkawkt'),
                  'fotospr' =>$gbr['file_name'],
				  'iddeveloper' =>$this->session->userdata('iddeveloper'),
				  'marketingdeveloper' =>$this->input->post('marketingdeveloper')
                );
				$this->db->where('nospr',$id); 
            $this->db->update('moncus_spr',$data); 
			}
		}
		else{
		$data = array(
				  'nospr' =>$this->input->post('nospr'),
				  'noktp' =>$this->input->post('noktp'),
				  'alamatobjek' =>$this->input->post('alamatobjek'),
				  'hargabangunan' =>$this->input->post('hargabangunan'),
				  'plafonbiaya' =>$this->input->post('plafonbiaya'),
				  'jangkawkt' =>$this->input->post('jangkawkt'),
                  'iddeveloper' =>$this->session->userdata('iddeveloper'),
				   'marketingdeveloper' =>$this->input->post('marketingdeveloper')
                );
				$this->db->where('nospr',$id); 
            $this->db->update('moncus_spr',$data); 
		}
               
        }
		function modelproseseditspradmin() { 
		
			$id = $this->input->post('nospr'); 
			$this->load->library('upload');
        $nmfile = 'spr_'.$this->input->post('nospr'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/spr/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotospr']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('fotospr'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'nospr' =>$this->input->post('nospr'),
				  'noktp' =>$this->input->post('noktp'),
				  'alamatobjek' =>$this->input->post('alamatobjek'),
				  'hargabangunan' =>$this->input->post('hargabangunan'),
				  'plafonbiaya' =>$this->input->post('plafonbiaya'),
				  'jangkawkt' =>$this->input->post('jangkawkt'),
                  'fotospr' =>$gbr['file_name'],
				  'iddeveloper' =>$this->input->post('iddeveloper'),
				  'marketingdeveloper' =>$this->input->post('marketingdeveloper')
                );
				$this->db->where('nospr',$id); 
            $this->db->update('moncus_spr',$data); 
			}
		}
		else{
		$data = array(
				  'nospr' =>$this->input->post('nospr'),
				  'noktp' =>$this->input->post('noktp'),
				  'alamatobjek' =>$this->input->post('alamatobjek'),
				  'hargabangunan' =>$this->input->post('hargabangunan'),
				  'plafonbiaya' =>$this->input->post('plafonbiaya'),
				  'jangkawkt' =>$this->input->post('jangkawkt'),
                  'iddeveloper' =>$this->input->post('iddeveloper'),
				   'marketingdeveloper' =>$this->input->post('marketingdeveloper')
                );
				$this->db->where('nospr',$id); 
            $this->db->update('moncus_spr',$data); 
		}
               
        }
		function modelproseseditkurang2() { 
		
			$id = $this->input->post('noktp'); 
			$this->load->library('upload');
        $nmfile = 'dk2_'.$this->input->post('noktp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/sppp/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['dk2']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('dk2'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'dk2' =>$gbr['file_name'],
				  );
				$this->db->where('noktp',$id); 
            $this->db->update('moncus_datakurang',$data); 
			}
		}
		else{
		$data = array(
				  'dk2' =>$gbr['file_name'],
				  );
				$this->db->where('noktp',$id); 
            $this->db->update('moncus_datakurang',$data); 
		}
               
        }
		function modelproseseditkurang3() { 
		
			$id = $this->input->post('noktp'); 
			$this->load->library('upload');
        $nmfile = 'dk3_'.$this->input->post('noktp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/datakurang/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['dk3']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('dk3'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'dk3' =>$gbr['file_name'],
				  );
				$this->db->where('noktp',$id); 
            $this->db->update('moncus_datakurang',$data); 
			}
		}
		else{
		$data = array(
				  'dk3' =>$gbr['file_name'],
				  );
				$this->db->where('noktp',$id); 
            $this->db->update('moncus_datakurang',$data); 
		}
               
        }
		function modelproseseditsp3() { 
		
			$id = $this->input->post('nosppp'); 
			$this->load->library('upload');
        $nmfile = 'sppp_'.$this->input->post('nosppp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/sppp/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotosppp']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('fotosppp'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'nosppp' =>$this->input->post('nosppp'),
				  'tglsppp' =>$this->input->post('tglsppp'),
				  'idanalisa' =>$this->input->post('idanalisa'),
				  'fotosppp' =>$gbr['file_name'],
				  );
				$this->db->where('nosppp',$id); 
            $this->db->update('moncus_sppp',$data); 
			}
		}
		else{
		$data = array(
				  'nosppp' =>$this->input->post('nosppp'),
				  'tglsppp' =>$this->input->post('tglsppp'),
				  'idanalisa' =>$this->input->post('idanalisa')
				  );
				$this->db->where('nosppp',$id); 
            $this->db->update('moncus_sppp',$data); 
		}
               
        }
		function modelproseseditkondisi() { 
		
			$id = $this->input->post('nokondisi'); 
			$this->load->library('upload');
        $nmfile = 'rumah_'.$this->input->post('nokondisi'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/kondisi/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['kondisi']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('kondisi'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'nokondisi' =>$this->input->post('nokondisi'),
				  'nospr' =>$this->input->post('nospr'),
				  'kondisi' =>$gbr['file_name']
                );
				$this->db->where('nokondisi',$id); 
            $this->db->update('moncus_kondisi',$data); 
			}
		}
		else{
		$data = array(
				  'nokondisi' =>$this->input->post('nokondisi'),
				  'nospr' =>$this->input->post('nospr')
				  );
				$this->db->where('nokondisi',$id); 
            $this->db->update('moncus_kondisi',$data); 
		}
               
        }
		function modelproseseditpricelist() { 
		
			$id = $this->input->post('no'); 
			$this->load->library('upload');
        $nmfile = 'pricelist_'.$this->input->post('no'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/pricelist/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['pricelist']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('pricelist'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'no' =>$this->input->post('no'),
				  'pricelist' =>$gbr['file_name']
                );
				$this->db->where('no',$id); 
            $this->db->update('moncus_pl',$data); 
			}
		}
		else{
		$data = array(
				  'no' =>$this->input->post('no')
				  );
				$this->db->where('no',$id); 
            $this->db->update('moncus_pl',$data); 
		}
               
        }
		function modelproseseditsyarat() { 
		
			$id = $this->input->post('no'); 
			$this->load->library('upload');
        $nmfile = 'syarat_'.$this->input->post('no'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/syarat/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['syarat']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('syarat'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'no' =>$this->input->post('no'),
				  'syarat' =>$gbr['file_name']
                );
				$this->db->where('no',$id); 
            $this->db->update('moncus_sypls',$data); 
			}
		}
		else{
		$data = array(
				  'no' =>$this->input->post('no')
				  );
				$this->db->where('no',$id); 
            $this->db->update('moncus_sypls',$data); 
		}
               
        }
		function modelproseseditshgb() { 
		
			$id = $this->input->post('nospr'); 
			$this->load->library('upload');
        $nmfile = 'shgb_'.$this->input->post('noshgb'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/sertifikat/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotoshgb']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('fotoshgb'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'noshgb' =>$this->input->post('noshgb'),
				  'nospr' =>$this->input->post('nospr'),
				  'fotoshgb' =>$gbr['file_name']
                );
				$this->db->where('nospr',$id); 
            $this->db->update('moncus_sertifikat',$data); 
			}
		}
		else{
		$data = array(
				  'noshgb' =>$this->input->post('noshgb'),
				  'nospr' =>$this->input->post('nospr')
				  );
				$this->db->where('nospr',$id); 
            $this->db->update('moncus_sertifikat',$data); 
		}
               
        }
		function modelproseseditdp() { 
		
			$id = $this->input->post('nodp'); 
			$this->load->library('upload');
        $nmfile = 'dp_'.$this->input->post('nodp'); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/kwitansi/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotokwitansi']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('fotokwitansi'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'nodp' =>$this->input->post('nodp'),
				  'nospr' =>$this->input->post('nospr'),
				  'tglbayardp1' =>$this->input->post('tglbayardp1'),
				  'nominaldp1' =>$this->input->post('nominaldp1'),
				  'tglbayardp2' =>$this->input->post('tglbayardp2'),
				  'nominaldp2' =>$this->input->post('nominaldp2'),
				  'tglbayardp3' =>$this->input->post('tglbayardp3'),
				  'nominaldp3' =>$this->input->post('nominaldp3'),
				  'fotokwitansi' =>$gbr['file_name']
				  );
				$this->db->where('nodp',$id); 
            $this->db->update('moncus_dp',$data); 
			}
		}
		else{
		$data = array(
				  'nodp' =>$this->input->post('nodp'),
				  'nospr' =>$this->input->post('nospr'),
				  'tglbayardp1' =>$this->input->post('tglbayardp1'),
				  'nominaldp1' =>$this->input->post('nominaldp1'),
				  'tglbayardp2' =>$this->input->post('tglbayardp2'),
				  'nominaldp2' =>$this->input->post('nominaldp2'),
				  'tglbayardp3' =>$this->input->post('tglbayardp3'),
				  'nominaldp3' =>$this->input->post('nominaldp3'));
				$this->db->where('nodp',$id); 
            $this->db->update('moncus_dp',$data); 
		}
               
        }
		
		function modelprosesinsertdp3() { 
		
			$id = $this->input->post('nodp'); 
			$this->load->library('upload');
        $nmfile = 'dp_'.$this->input->post('nodp');//nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/kwitansi/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotokwitansi']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('fotokwitansi'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'nodp' =>$this->input->post('nodp'),
				  'tglbayardp3' =>$this->input->post('tglbayardp3'),
				  'nominaldp3' =>$this->input->post('nominaldp3'),
				  'fotokwitansi' =>$gbr['file_name']
				  );
				$this->db->where('nodp',$id); 
            $this->db->update('moncus_dp',$data); 
			}
		}
		else{
		$data = array(
				  'nodp' =>$this->input->post('nodp'),
				  'tglbayardp3' =>$this->input->post('tglbayardp3'),
				  'nominaldp3' =>$this->input->post('nominaldp3'));
				$this->db->where('nodp',$id); 
            $this->db->update('moncus_dp',$data);  
		}
               
        }
		function modelprosesinsertdp2() { 
		
			$id = $this->input->post('nodp'); 
			$this->load->library('upload');
        $nmfile = 'dp2_'.time();//nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/kwitansi/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotokwitansi2']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('fotokwitansi2'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'nodp' =>$this->input->post('nodp'),
				  'tglbayardp2' =>$this->input->post('tglbayardp2'),
				  'nominaldp2' =>$this->input->post('nominaldp2'),
				  'fotokwitansi2' =>$gbr['file_name']
				  );
				$this->db->where('nodp',$id); 
            $this->db->update('moncus_dp',$data); 
			}
		}
		else{
		$data = array(
				  'nodp' =>$this->input->post('nodp'),
				  'tglbayardp2' =>$this->input->post('tglbayardp2'),
				  'nominaldp2' =>$this->input->post('nominaldp2'));
				$this->db->where('nodp',$id); 
            $this->db->update('moncus_dp',$data); 
		}
               
        }
	function modelprosesinsertimb() { 
		
			
			$this->load->library('upload');
			$id = $this->input->post('nospr'); 
        $nmfile = 'imb_'.$this->input->post('noimb');//nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/archive/imb/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '10000'; //lebar maksimum 1288 px
        $config['max_height']  = '10000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
        if($_FILES['fotoimb']['tmp_name'] != '')
        {
            if ($this->upload->do_upload('fotoimb'))
            {
                $gbr = $this->upload->data();
                
				$data = array(
				  'noimb' =>$this->input->post('noimb'),
				  'fotoimb' =>$gbr['file_name']
				  );
				$this->db->where('nospr',$id); 
            $this->db->update('moncus_sertifikat',$data); 
			}
		}
		else{
		$data = array(
				  'noimb' =>$this->input->post('noimb'));
				$this->db->where('nospr',$id); 
            $this->db->update('moncus_sertifikat',$data);   
		}
               
        }

}

?>
